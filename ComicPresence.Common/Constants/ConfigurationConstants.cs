﻿using System;

namespace ComicPresence.Common.Constants
{
    public static class ConfigurationConstants
    {
        public static string AdminAvatarSize = "AdminAvatarSize";
        public static string Characters = "Characters";
        public static string CharacterThumbs = "CharacterThumbs";
        public static string ComicBanner = "ComicBanner";
        public static string ComicPath = "ComicPath";
        public static string ComicThumbnailPath = "ComicThumbnailPath";
        public static string DefaultPostTime = "DefaultPostTime";
        public static string LinkImages = "LinkImages";
        public static string Remember = "Remember";
        public static string SiteName = "siteName";
        public static string SiteSlogan = "siteSlogan";
        public static string SiteTheme = "SiteTheme";
        public static string SiteVersion = "siteVersion";
        public static string UserAvatar = "UserAvatar";
    }
}
