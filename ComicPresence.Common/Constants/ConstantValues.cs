﻿using System;

namespace ComicPresence.Common.Constants
{
    public static class ConstantValues
    {
        public static string AddStatus = "AddStatus";
        public static string Administrator = "Administrator";
        public static string ApplicationName = "/ComicPresense";
        public static string Artist = "artist";
        public static string Authors = "Authors";
        public static string AuthorSelectedType = "AuthorType";
        public static string Book = "Book";
        public static string Books = "books";
        public static string BookChapter = "BookChapter";
        public static string CastIds = "comicCast";
        public static string Chapter = "Chapter";
        public static string ChapterId = "chapterId";
        public static string CharacterName = "Exists";
        public static string ComicCast = "comicCast";
        public static string ComicId = "comicId";
        public static string ContentEditor = "Content Editor";
        public static string DefaultAvatar = "~/images/dash/avatars/avatar.jpg";
        public static string Email = "Membership.Email";
        public static string Exists = "Exists";
        public static string Fail = "fail";
        public static string Female = "F";
        public static string FirstName = "Author.FirstName";
        public static string Gallery = "~/images/gallery/";
        public static string GravatarUrl = "http://www.gravatar.com/avatar/";
        public static string Gender = "gender";
        public static string IsArchived = "IsArchived";
        public static string IsLastMix = "IsLastMix";
        public static string LastName = "Author.LastName";
        public static string LinkImages = "LinkImages";
        public static string InvalidFormData = "invalid";
        public static string LongDescription = "LongDescription";
        public static string Male = "M";
        public static string NewsExists = "News article title exists";
        public static string OnHomePage = "OnHomePage";
        public static string Password = "Membership.Password";
        public static string Ready = "Ready";
        public static string RoleNotSelected = "role not selected";
        public static string ShortDescription = "ShortDescription";
        public static string Success = "success";
        public static string Tags = "tags";
        public static string ThumbNails = "~/images/gallery/thumbnails/";
        public static string UseGravatar = "Author.UseGravatar";
        public static string UserName = "User.UserName";
        public static string UpdateStatus = "UpdateStatus";
        public static string Writer = "writer";
    }
}
