﻿using System.Text.RegularExpressions;

namespace ComicPresence.Common.Infrastructure
{
    public interface IRegularExpressions
    {
        Regex GetExpression(string expressionName);
        bool IsMatch(string expressionName, string input);
        string Clean(string expressionName, string input);
    }
}
