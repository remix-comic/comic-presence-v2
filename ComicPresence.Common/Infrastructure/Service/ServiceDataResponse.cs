﻿using System;
using System.Runtime.Serialization;

namespace ComicPresence.Common.Infrastructure.Service
{
    public class ServiceDataResponse<T> : ServiceResponse
    {
        #region Instance Fields
        [DataMember]
        public T cpObject;
        #endregion Instance Fields

        public ServiceDataResponse()
        {
        }

        public ServiceDataResponse(T d)
        {
            this.success = true;
            cpObject = d;
        }
    }
}
