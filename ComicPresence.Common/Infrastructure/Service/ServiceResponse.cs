﻿using System;
using System.Runtime.Serialization;

namespace ComicPresence.Common.Infrastructure.Service
{
    [DataContract]
    public class ServiceResponse
    {
        [DataMember]
        public bool success;
        [DataMember]
        public string message;
    }
}
