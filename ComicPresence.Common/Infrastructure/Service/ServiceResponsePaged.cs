﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicPresence.Common.Infrastructure.Service
{
    [DataContract]
    public class ServiceResponsePaged<T> : ServiceResponse
    {
        #region Instance Fields
        [DataMember]
        public int totalItems;
        [DataMember]
        public int totalPages;
        #endregion Instance Fields

        public ServiceResponsePaged()
        {
        }

        public ServiceResponsePaged(int totalItems, int totalPages, IEnumerable<T> l)
        {
            this.success = true;
            this.totalItems = totalItems;
            this.totalPages = totalPages;
        }
    }
}
