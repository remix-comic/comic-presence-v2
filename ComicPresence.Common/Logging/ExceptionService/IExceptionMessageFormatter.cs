﻿using System;

namespace ComicPresence.Common.Logging.ExceptionService
{
    public interface IExceptionMessageFormatter
    {
        string GetEntireExceptionStack(Exception ex);
    }
}
