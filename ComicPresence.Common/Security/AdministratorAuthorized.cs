﻿using System.Web.Http;

namespace ComicPresence.Common.Security
{
    public class AdministratorAuthorized : AuthorizeAttribute
    {
        public AdministratorAuthorized()
        {
            Roles = "Administrator";
        }
    }
}
