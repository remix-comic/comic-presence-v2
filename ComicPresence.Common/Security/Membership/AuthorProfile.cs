﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ComicPresence.Common.Security.Membership
{
    [Table("AuthorProfile")]
    public class AuthorProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int AuthorId { get; set; }
        public string UserName { get; set; }
    }
}
