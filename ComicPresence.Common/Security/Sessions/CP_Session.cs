﻿using System;
using System.Web;
using System.Web.Security;

using ComicPresence.Common.Logging;

namespace ComicPresence.Common.Security.Sessions
{
    [Serializable]
    public class CP_Session
    {
        public int AuthorId { get; set; }
        public string Avatar { get; set; }
        public int BlogId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid UserId { get; set; }
        public int Vistied { get; set; }        

        // Default Constructor
        public CP_Session()
        {
        }

        /// <summary>
        /// Create object of Comic Presence 
        /// </summary>
        /// <param name="avatar"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="userId"></param>
        public CP_Session(int authorId, int blogId, string avatar, string firstName, string lastName, Guid userId, int visited)
        {
            this.AuthorId = authorId;
            this.Avatar = avatar;
            this.BlogId = blogId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.UserId = userId;
            this.Vistied = visited;
        }

        /// <summary>
        /// Log off the current user.
        /// </summary>
        /// <param name="httpSession">The current HTTP session.</param>
        public static void LogOff(HttpSessionStateBase httpSession)
        {
            //
            // Write in the event log the message about the user's Log Off.
            // Note that could be situations that this code was invoked from "Error" page 
            // after the current user session has expired, or before the user to login!
            //
            Logger _logger = new Logger();
            CP_Session siteSession = (httpSession["AdminUser"] == null ? null : (CP_Session)httpSession["AdminUser"]);

            if (siteSession != null)
                _logger.Info(string.Format("Log off for user: {0} {1}", siteSession.FirstName, siteSession.LastName));

            //
            // Log off the current user and clears its site session cache.
            //

            FormsAuthentication.SignOut();

            httpSession["AdminUser"] = null;
        }
    }
}
