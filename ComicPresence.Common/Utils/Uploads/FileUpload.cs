﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

using ComicPresence.Common.Logging;
using ComicPresence.Common.Constants;

namespace ComicPresence.Common.Utils.Uploads
{
    public static class FileUpload
    {
        private static string SaveFileName { get; set; }
        private static Logger ExceptionLoggingService = new Logger();

        private static string CharacterPath
        {
            get { return ConfigurationManager.AppSettings["Characters"].ToString(); }
        }

        private static string CharacterPathThumb
        {
            get { return ConfigurationManager.AppSettings["CharacterThumbs"].ToString(); }
        }

        private static string ComicBannerPath
        {
            get { return ConfigurationManager.AppSettings["ComicBanner"].ToString(); }
        }

        private static string ComicFilePath
        {
            get { return ConfigurationManager.AppSettings["ComicPath"].ToString(); }
        }

        private static string ComicThumbnailPath
        {
            get { return ConfigurationManager.AppSettings["ComicThumbnailPath"].ToString(); }
        }

        private static string LinkPath
        {
            get { return ConfigurationManager.AppSettings["LinkImages"].ToString(); }
        }

        private static string UserAvatarPath
        {
            get { return ConfigurationManager.AppSettings["UserAvatar"].ToString(); }
        }

        public static string SaveCharacter(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(CharacterPath + "/" + SaveFileName));
                return CharacterPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Character image upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveCharacterThumb(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(CharacterPathThumb + "/" + SaveFileName));
                return CharacterPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Character thumbnail upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveComic(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(ComicFilePath + "/" + SaveFileName));
                return ComicFilePath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Comic image upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveComicBanner(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(ComicBannerPath + "/" + SaveFileName));
                return ComicBannerPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Comic banner upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveComicThumbNail(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(ComicThumbnailPath + "/" + SaveFileName));
                return ComicThumbnailPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Comic thumbnail upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveGalleryImage(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(ConstantValues.Gallery + SaveFileName));
                return ConstantValues.Gallery + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Gallery image upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveLink(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(LinkPath + "/" + SaveFileName));
                return LinkPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Link image upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveUserAvatar(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(UserAvatarPath + "/" + SaveFileName));
                return UserAvatarPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Avatar upload failed " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }

        public static string SaveUserGravatar(HttpPostedFileBase file, string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException("file");

            try
            {
                return ConstantValues.GravatarUrl + email;
            }
            catch (Exception e)
            {
                ExceptionLoggingService.Error("Invalid Gravatar account " + DateTime.UtcNow);
                return ConstantValues.Fail;
            }
        }
    }
}
