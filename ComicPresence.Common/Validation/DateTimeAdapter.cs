﻿using System;

namespace ComicPresence.Common.Validation
{
    public class DateTimeAdapter : IDateTime
    {
        public DateTime UtcNow
        {
            get
            {
                return DateTime.UtcNow;
            }
        }
    }
}
