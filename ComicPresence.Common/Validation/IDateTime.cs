﻿using System;

namespace ComicPresence.Common.Validation
{
    public interface IDateTime
    {
        DateTime UtcNow { get; }
    }
}
