﻿
namespace ComicPresence.Common.Validation
{
    public interface IValidationService
    {
        ValidationState Validate<T>(T model);
    }
}
