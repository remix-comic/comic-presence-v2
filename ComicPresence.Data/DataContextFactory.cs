﻿using ComicPresence.Infrastructure.DataContextStorage;

namespace ComicPresence.Data
{
    /// <summary>
    /// Manages instances of the ContactManagerContext and stores them in an appropriate storage container.
    /// </summary>
    public static class DataContextFactory
    {
        /// <summary>
        /// Clears out the current ContactManagerContext.
        /// </summary>
        public static void Clear()
        {
            var dataContextStorageContainer = DataContextStorageFactory<CmsContext>.CreateStorageContainer();
            dataContextStorageContainer.Clear();
        }

        /// <summary>
        /// Retrieves an instance of ContactManagerContext from the appropriate storage container or
        /// creates a new instance and stores that in a container.
        /// </summary>
        /// <returns>An instance of ContactManagerContext.</returns>
        public static CmsContext GetDataContext()
        {
            var dataContextStorageContainer = DataContextStorageFactory<CmsContext>.CreateStorageContainer();
            var cmsManagerContext = dataContextStorageContainer.GetDataContext();

            if (cmsManagerContext == null)
            {
                cmsManagerContext = new CmsContext();
                dataContextStorageContainer.Store(cmsManagerContext);
            }

            return cmsManagerContext;
        }
    }
}
