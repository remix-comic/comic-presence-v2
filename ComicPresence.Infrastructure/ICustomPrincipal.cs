﻿using System;
using System.Security.Principal;

namespace ComicPresence.Infrastructure
{
    public interface ICustomPrincipal : IPrincipal
    {
        int AuthorId { get; set; }
        Guid UserId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string FullName { get; set; }
        string Avatar { get; set; }
    }
}
