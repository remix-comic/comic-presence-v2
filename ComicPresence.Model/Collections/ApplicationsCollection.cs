﻿using System;
using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of aspnet_Application instances in the system.
    /// </summary>
    public class ApplicationsCollection : CollectionBase<dtoaspnet_Applications>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Applications"/> class.
        /// </summary>
        public ApplicationsCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Application"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of aspnet_Application as the initial list.</param>
        public ApplicationsCollection(IList<dtoaspnet_Applications> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Application"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of aspnet_Application as the initial list.</param>
        public ApplicationsCollection(CollectionBase<dtoaspnet_Applications> initialList)
            : base(initialList)
        {
        }
    }
}
