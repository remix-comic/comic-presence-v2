﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Author instances in the system.
    /// </summary>
    public class AuthorCollection : CollectionBase<dtoAuthor>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Authors"/> class.
        /// </summary>
        public AuthorCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Authors"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Author as the initial list.</param>
        public AuthorCollection(IList<dtoAuthor> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Authors"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Author as the initial list.</param>
        public AuthorCollection(CollectionBase<dtoAuthor> initialList)
            : base(initialList)
        {
        }
    }
}
