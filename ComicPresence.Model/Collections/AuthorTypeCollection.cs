﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of AuthorType instances in the system.
    /// </summary>
    public class AuthorTypeCollection : CollectionBase<dtoAuthorType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorTypes"/> class.
        /// </summary>
        public AuthorTypeCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorTypes"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of AuthorType as the initial list.</param>
        public AuthorTypeCollection(IList<dtoAuthorType> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorTypes"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of AuthorType as the initial list.</param>
        public AuthorTypeCollection(CollectionBase<dtoAuthorType> initialList)
            : base(initialList)
        {
        }
    }
}
