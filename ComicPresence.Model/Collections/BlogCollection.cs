﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Blog instances in the system.
    /// </summary>
    public class BlogCollection : CollectionBase<dtoBlog>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Blogs"/> class.
        /// </summary>
        public BlogCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Blogs"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Blog as the initial list.</param>
        public BlogCollection(IList<dtoBlog> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Blogs"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Blog as the initial list.</param>
        public BlogCollection(CollectionBase<dtoBlog> initialList)
            : base(initialList)
        {
        }
    }
}
