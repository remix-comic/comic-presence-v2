﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of BloodType instances in the system.
    /// </summary>
    public class BloodTypeCollection : CollectionBase<dtoBloodType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BloodTypes"/> class.
        /// </summary>
        public BloodTypeCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BloodTypes"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of BloodType as the initial list.</param>
        public BloodTypeCollection(IList<dtoBloodType> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BloodTypes"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of BloodType as the initial list.</param>
        public BloodTypeCollection(CollectionBase<dtoBloodType> initialList)
            : base(initialList)
        {
        }
    }
}
