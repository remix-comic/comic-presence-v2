﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of BookChapter instances in the system.
    /// </summary>
    public class BookChapterCollection : CollectionBase<dtoBookChapter>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookChapters"/> class.
        /// </summary>
        public BookChapterCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BookChapters"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of BookChapter as the initial list.</param>
        public BookChapterCollection(IList<dtoBookChapter> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BookChapters"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of BookChapter as the initial list.</param>
        public BookChapterCollection(CollectionBase<dtoBookChapter> initialList)
            : base(initialList)
        {
        }
    }
}
