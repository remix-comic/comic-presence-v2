﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Book instances in the system.
    /// </summary>
    public class BookCollection : CollectionBase<dtoBook>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Books"/> class.
        /// </summary>
        public BookCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Books"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Book as the initial list.</param>
        public BookCollection(IList<dtoBook> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Books"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Book as the initial list.</param>
        public BookCollection(CollectionBase<dtoBook> initialList)
            : base(initialList)
        {
        }
    }
}
