﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Cast instances in the system.
    /// </summary>
    public class CastCollection : CollectionBase<dtoCast>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Casts"/> class.
        /// </summary>
        public CastCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Casts"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Cast as the initial list.</param>
        public CastCollection(IList<dtoCast> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Casts"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Cast as the initial list.</param>
        public CastCollection(CollectionBase<dtoCast> initialList)
            : base(initialList)
        {
        }
    }
}
