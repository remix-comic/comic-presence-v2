﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Category instances in the system.
    /// </summary>
    public class CategoryCollection : CollectionBase<dtoCategory>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Categories"/> class.
        /// </summary>
        public CategoryCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Categories"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Category as the initial list.</param>
        public CategoryCollection(IList<dtoCategory> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Categories"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Category as the initial list.</param>
        public CategoryCollection(CollectionBase<dtoCategory> initialList)
            : base(initialList)
        {
        }
    }
}
