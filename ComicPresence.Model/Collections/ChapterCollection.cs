﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Chapter instances in the system.
    /// </summary>
    public class ChapterCollection : CollectionBase<dtoChapter>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Chapters"/> class.
        /// </summary>
        public ChapterCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Chapters"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Chapter as the initial list.</param>
        public ChapterCollection(IList<dtoChapter> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Chapters"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Chapter as the initial list.</param>
        public ChapterCollection(CollectionBase<dtoChapter> initialList)
            : base(initialList)
        {
        }

        public IEnumerable<ValidationResult> Validate()
        {
            var errors = new List<ValidationResult>();

            foreach (var chapter in this)
            {
                errors.AddRange(chapter.Validate());
            }

            return errors;
        }

    }
}
