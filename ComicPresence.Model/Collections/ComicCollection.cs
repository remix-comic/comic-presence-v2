﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Comic instances in the system.
    /// </summary>
    public class ComicCollection : CollectionBase<dtoComic>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Comic"/> class.
        /// </summary>
        public ComicCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Comic"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Comic as the initial list.</param>
        public ComicCollection(IList<dtoComic> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Comic"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Comic as the initial list.</param>
        public ComicCollection(CollectionBase<dtoComic> initialList)
            : base(initialList)
        {
        }
    }
}
