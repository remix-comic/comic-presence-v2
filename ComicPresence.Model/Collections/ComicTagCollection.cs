﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of ComicTag instances in the system.
    /// </summary>
    public class ComicTagCollection : CollectionBase<dtoComicTag>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComicTags"/> class.
        /// </summary>
        public ComicTagCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComicTags"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of ComicTag as the initial list.</param>
        public ComicTagCollection(IList<dtoComicTag> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComicTags"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of ComicTag as the initial list.</param>
        public ComicTagCollection(CollectionBase<dtoComicTag> initialList)
            : base(initialList)
        {
        }
    }
}
