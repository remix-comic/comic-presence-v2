﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Configuration instances in the system.
    /// </summary>
    public class ConfigurationCollection : CollectionBase<dtoConfiguration>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Configurations"/> class.
        /// </summary>
        public ConfigurationCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Configurations"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Configuration as the initial list.</param>
        public ConfigurationCollection(IList<dtoConfiguration> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Configurations"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Configuration as the initial list.</param>
        public ConfigurationCollection(CollectionBase<dtoConfiguration> initialList)
            : base(initialList)
        {
        }
    }
}
