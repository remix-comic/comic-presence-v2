﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Link instances in the system.
    /// </summary>
    public class LinkCollection : CollectionBase<dtoLink>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Links"/> class.
        /// </summary>
        public LinkCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Links"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Link as the initial list.</param>
        public LinkCollection(IList<dtoLink> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Links"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Link as the initial list.</param>
        public LinkCollection(CollectionBase<dtoLink> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Validates the current collection by validating each individual item in the collection.
        /// </summary>
        /// <returns>A IEnumerable of ValidationResult. The IEnumerable is empty when the object is in a valid state.</returns>
        public IEnumerable<ValidationResult> Validate()
        {
            var errors = new List<ValidationResult>();

            foreach (var link in this)
            {
                errors.AddRange(link.Validate());
            }

            return errors;
        }
    }
}
