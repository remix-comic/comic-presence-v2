﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of MediaLibrary instances in the system.
    /// </summary>
    public class MedialLibraryColleciton : CollectionBase<dtoMediaLibrary>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MediaLibraries"/> class.
        /// </summary>
        public MedialLibraryColleciton()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaLibraries"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of MediaLibrary as the initial list.</param>
        public MedialLibraryColleciton(IList<dtoMediaLibrary> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaLibraries"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of MediaLibrary as the initial list.</param>
        public MedialLibraryColleciton(CollectionBase<dtoMediaLibrary> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Validates the current collection by validating each individual item in the collection.
        /// </summary>
        /// <returns>A IEnumerable of ValidationResult. The IEnumerable is empty when the object is in a valid state.</returns>
        public IEnumerable<ValidationResult> Validate()
        {
            var errors = new List<ValidationResult>();

            foreach (var mediaLib in this)
            {
                errors.AddRange(mediaLib.Validate());
            }

            return errors;
        }
    }
}
