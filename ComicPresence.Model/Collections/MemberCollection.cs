﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Membership instances in the system.
    /// </summary>
    public class MemberCollection : CollectionBase<dtoaspnet_Membership>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Membership"/> class.
        /// </summary>
        public MemberCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Membership"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of aspnet_Membership as the initial list.</param>
        public MemberCollection(IList<dtoaspnet_Membership> initalList)
            : base(initalList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Membership"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of aspnet_Membership as the initial list.</param>
        public MemberCollection(CollectionBase<dtoaspnet_Membership> initalList)
            : base(initalList)
        {
        }
    }
}
