﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of aspnet_Profile instances in the system.
    /// </summary>
    public class ProfileCollection : CollectionBase<dtoaspnet_Profile>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Profiles"/> class.
        /// </summary>
        public ProfileCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Profiles"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of aspnet_Profile as the initial list.</param>
        public ProfileCollection(IList<dtoaspnet_Profile> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Profiles"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of aspnet_Profile as the initial list.</param>
        public ProfileCollection(CollectionBase<dtoaspnet_Profile> initialList)
            : base(initialList)
        {
        }
    }
}
