﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of aspnet_Role instances in the system.
    /// </summary>
    public class RoleCollection : CollectionBase<dtoaspnet_Roles>
    {        
        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Roles"/> class.
        /// </summary>
        public RoleCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Roles"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of aspnet_Role as the initial list.</param>
        public RoleCollection(IList<dtoaspnet_Roles> initalList)
            : base(initalList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Roles"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of aspnet_Role as the initial list.</param>
        public RoleCollection(CollectionBase<dtoaspnet_Roles> initalList)
            : base(initalList)
        {
        }

        /// <summary>
        /// Validates the current collection by validating each individual item in the collection.
        /// </summary>
        /// <returns>A IEnumerable of ValidationResult. The IEnumerable is empty when the object is in a valid state.</returns>
        public IEnumerable<ValidationResult> Validate()
        {
            var errors = new List<ValidationResult>();

            foreach (var role in this)
            {
                errors.AddRange(role.Validate());
            }

            return errors;
        }
    }
}
