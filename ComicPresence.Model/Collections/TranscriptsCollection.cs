﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of Transcript instances in the system.
    /// </summary>
    public class TranscriptsCollection : CollectionBase<dtoTranscript>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Transcript"/> class.
        /// </summary>
        public TranscriptsCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Transcripts"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of Transcript as the initial list.</param>
        public TranscriptsCollection(IList<dtoTranscript> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Transcripts"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of Transcript as the initial list.</param>
        public TranscriptsCollection(CollectionBase<dtoTranscript> initialList)
            : base(initialList)
        {
        }
    }
}
