﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of aspnet_User instances in the system.
    /// </summary>
    public class UsersCollection : CollectionBase<dtoaspnet_Users>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Users"/> class.
        /// </summary>
        public UsersCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Users"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of aspnet_User as the initial list.</param>
        public UsersCollection(IList<dtoaspnet_Users> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_Users"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of aspnet_User as the initial list.</param>
        public UsersCollection(CollectionBase<dtoaspnet_Users> initialList)
            : base(initialList)
        {
        }
    }
}
