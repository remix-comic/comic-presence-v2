﻿using System.Collections.Generic;

namespace ComicPresence.Model.Collections
{
    /// <summary>
    /// Represents a collection of aspnet_UsersInRole instances in the system.
    /// </summary>
    public class UsersInRolesCollection : CollectionBase<dtoaspnet_UsersInRoles>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_UsersInRoles"/> class.
        /// </summary>
        public UsersInRolesCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_UsersInRoles"/> class.
        /// </summary>
        /// <param name="initialList">Accepts an IList of aspnet_UsersInRole as the initial list.</param>
        public UsersInRolesCollection(IList<dtoaspnet_UsersInRoles> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="aspnet_UsersInRoles"/> class.
        /// </summary>
        /// <param name="initialList">Accepts a CollectionBase of aspnet_UsersInRole as the initial list.</param>
        public UsersInRolesCollection(CollectionBase<dtoaspnet_UsersInRoles> initialList)
            : base(initialList)
        {
        }
    }
}
