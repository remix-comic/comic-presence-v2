﻿
namespace ComicPresence.Model.Enums
{
    public enum AuthType
    {
        Illustrator = 1,
        Writer = 2,
        Both = 3
    }
}
