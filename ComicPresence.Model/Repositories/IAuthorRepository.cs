﻿using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    /// <summary>
    /// Defines the various methods available to work with Authors in the system.
    /// </summary>
    public interface IAuthorRepository : IRepository<dtoAuthor, int>
    {
        /// <summary>
        /// Adds a new object of Author to the system
        /// </summary>
        /// <param name="author">dtoAuthor</param>
        /// <returns>dtoAuthor.Id</returns>
        int AddReturnId (dtoAuthor author);
    }
}
