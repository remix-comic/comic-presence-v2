﻿using System;
using System.Linq;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    /// <summary>
    /// Defines the various methods available to work with author types in the system.
    /// </summary>
    public interface IAuthorTypeRepository : IRepository<dtoAuthorType, int>
    {
    }
}
