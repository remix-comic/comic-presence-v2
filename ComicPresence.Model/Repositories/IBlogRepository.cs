﻿using System.Collections.Generic;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    /// <summary>
    /// Defines the various methods available to work with blogs in the system.
    /// </summary>
    public interface IBlogRepository : IRepository<dtoBlog, int>
    {
        /// <summary>
        /// Adds an new blog to system.
        /// </summary>
        /// <param name="blog">dtoBlog</param>
        /// <returns>dtoBlog.Id</returns>
        int AddReturnId(dtoBlog blog);

        /// <summary>
        /// Gets a list of all the blogs in the CMS by author id.
        /// </summary>
        /// <param name="authorId"></param>
        /// <returns>List of <see cref="dtoBlog"/></returns>
        List<dtoBlog> Blogs(int authorId);

        /// <summary>
        /// Gets a blog by it's slug.
        /// </summary>
        /// <param name="slug"></param>
        /// <returns><see cref="dtoBlog"/></returns>
        dtoBlog Blog(string slug);
    }
}
