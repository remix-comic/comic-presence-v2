﻿using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IBookChapterRepository : IRepository<dtoBookChapter, int>
    {
    }
}
