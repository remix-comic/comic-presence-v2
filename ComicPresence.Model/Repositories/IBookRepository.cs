﻿using System;
using System.Linq;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IBookRepository : IRepository<dtoBook, int>
    {
        int AddReturnId(dtoBook book);
    }
}
