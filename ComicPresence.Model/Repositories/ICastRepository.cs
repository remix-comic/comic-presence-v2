﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface ICastRepository : IRepository<dtoCast, int>
    {
        int AddReturnId(dtoCast cast, HttpPostedFileBase imageUpload, HttpPostedFileBase thumbNailUpload);
        IEnumerable<dtoCast> GetAll(string sortOrder);
        string Update(dtoCast cast, HttpPostedFileBase imageUpload, HttpPostedFileBase thumbNailUpload);
    }
}
