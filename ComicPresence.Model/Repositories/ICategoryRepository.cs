﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface ICategoryRepository : IRepository<dtoCategory, int>
    {
        int AddReturnId(dtoCategory entity);
        IEnumerable<dtoCategory> Categories();
        IEnumerable<dtoCategory> Categories(int take);
        IEnumerable<dtoCategory> Categories(string name);
        dtoCategory Categoy(string name);
        IEnumerable<dtoCategory> MostUsed();
    }
}
