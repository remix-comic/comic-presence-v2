﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IChapterRepository : IRepository<dtoChapter, int>
    {
        int AddReturnId(dtoChapter chapter, int bookId);
    }
}
