﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IComicCastRepository : IRepository<dtoComicCast, int>
    {
        int AddReturnId(dtoComicCast comicCast);
    }
}
