﻿using System.Collections.Generic;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    /// <summary>
    /// Defines the various methods available to work with comics in the system.
    /// </summary>
    public interface IComicRepository : IRepository<dtoComic, int>
    {
        /// <summary>
        /// Gets a list of all the comics in the CMS.
        /// </summary>
        /// <returns>An IEnumerable of dtoComic.</returns>
        IEnumerable<dtoComic> GetAllComics();

        /// <summary>
        /// Get a comic by it's Internet slug.
        /// </summary>
        /// <param name="slug"></param>
        /// <returns></returns>
        dtoComic GetComicBySlug(string slug);

        dtoComic GetCurrentComic();
    }
}
