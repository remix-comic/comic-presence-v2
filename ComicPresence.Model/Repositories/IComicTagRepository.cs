﻿using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IComicTagRepository : IRepository<dtoComicTag, int>
    {
    }
}
