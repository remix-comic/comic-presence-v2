﻿using System;
using System.Web;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface ILinkRepository : IRepository<dtoLink, int>
    {
        int AddReturnId(dtoLink link, HttpPostedFileBase imagePath);
        void Update(dtoLink link, HttpPostedFileBase imagePath);
    }
}
