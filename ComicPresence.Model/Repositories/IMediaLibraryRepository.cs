﻿using System;
using System.Web;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IMediaLibraryRepository : IRepository<dtoMediaLibrary, int>
    {
        int AddReturnId(dtoMediaLibrary library);
        int AddReturnId(dtoMediaLibrary library, HttpPostedFileBase file);
        dtoMediaLibrary GetLibraryBySlug(string mediaLibrary);
        void Update(dtoMediaLibrary library, HttpPostedFileBase file);
    }
}
