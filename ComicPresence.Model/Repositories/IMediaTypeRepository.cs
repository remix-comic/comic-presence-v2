﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IMediaTypeRepository : IRepository<dtoMediaType, int>
    {
    }
}
