﻿using System;
using System.Collections.Generic;
using System.Web;

using ComicPresence.Common.Security.Membership;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IMembershipRepository : IRepository<dtoaspnet_Membership, int>
    {
        bool AddReturnBool(dtoaspnet_Users user);
        IEnumerable<dtoaspnet_Membership> GetAllAuthors();
        bool CheckEmail(string email);
        bool DeleteReturnBool(int id);
        dtoaspnet_Applications GetApplicationInfo();
        string GetUserName(string userName);
        string UpdateReturnString(dtoaspnet_Membership author, HttpPostedFileBase file);
        bool ValidateUser(LoginModel login);
    }
}
