﻿using System;
using System.Collections.Generic;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IMenuRepository : IRepository<dtoMenu, int>
    {
        dtoMenu GetAdminMenu();
    }
}
