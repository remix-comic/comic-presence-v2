﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

using ComicPresence.Common.Pager.NewsPager;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface INewsRepository : IRepository<dtoNews, int>
    {
        IList<dtoNews> Get();
        PagedList<dtoNews> GetNewsFeed(int pageIndex, int pageSize, int? totalCount);
    }
}
