﻿using System;
using System.Collections.Generic;

using ComicPresence.Common.Pager;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IPostRepository : IRepository<dtoPost, int>
    {
        bool AddReturnBool(dtoPost entity, string tags);
        IEnumerable<dtoPostArchive> Archives();
        IEnumerable<dtoPostArchive> Archives(int year, string month);
        bool Delete(int id);
        bool DeleteRetValue(dtoPost entity);
        IEnumerable<dtoPost> GetAll(int? count);
        dtoNewPost New();
        dtoPost Post(int id, int blogid);
        dtoPost Post(int id);
        dtoPost Post(string slug);
        PagedList<dtoPost> Posts(int id, int pageNo, int pageSize);
        IEnumerable<dtoPost> Posts(int year, string month);
        bool Update(dtoPost entity, string tags);
    }
}
