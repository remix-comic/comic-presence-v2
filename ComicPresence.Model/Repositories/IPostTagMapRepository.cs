﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IPostTagMapRepository : IRepository<dtoPostTagMap, int>
    {
        bool AddReturnBool(dtoPostTagMap entity);
    }
}
