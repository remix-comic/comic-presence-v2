﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IRoleRepository : IRepository<dtoaspnet_Roles, int>
    {
    }
}
