﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface ISiteSettingsRepository : IRepository<dtoSiteSetting, int>
    {
    }
}
