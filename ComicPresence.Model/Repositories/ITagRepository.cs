﻿using System;
using System.Collections.Generic;

using ComicPresence.Common.Pager;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface ITagRepository : IRepository<dtoTag, int>
    {
        bool AddReturnBool(string newTags);
        bool Delete(int Id);
        bool Update(dtoTag tag);
        IEnumerable<dtoTag> MostUsed();
        IEnumerable<dtoTag> Tags();
        IEnumerable<dtoTag> TagCloud();
        PagedList<dtoTag> Tags(int pageIndex, int pageSize);
    }
}
