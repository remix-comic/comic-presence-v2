﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface ITranscriptRepository : IRepository<dtoTranscript, int>
    {
        bool AddReturnBool(dtoTranscript transcript);
    }
}
