﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IUserService : IRepository<dtoaspnet_Users, int>
    {
    }
}
