﻿using System;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model.Repositories
{
    public interface IUsersInRolesRepository : IRepository<dtoaspnet_UsersInRoles, int>
    {
    }
}
