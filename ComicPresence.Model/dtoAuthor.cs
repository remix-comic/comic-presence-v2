using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoAuthor : DomainEntity<int>
    {
        public int AuthorId { get; set; }
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        public int AuthorType { get; set; }
        public string TypeName { get; set; }
        public string Avatar { get; set; }
        public bool UseGravatar { get; set; }
        public dtoAuthorType AuthorTypes { get; set; }
        public int ComicCount { get; set; }
        public virtual ICollection<dtoComic> Comics { get; set; }
        public virtual ICollection<dtoBook> Books { get; set; }
        public string FullName { get; set; }

        public dtoAuthor()
        {
            this.Books = new List<dtoBook>();
            this.Comics = new List<dtoComic>();
        }

        public dtoAuthor(int authorId, Guid applicationId, string applicationName, string loweredApplicationName, int authorType, string avatar, bool useGravatar, dtoAuthorType authorTypes, int comicCount, List<dtoComic> comics, List<dtoBook> books, string fullName)
        {
			this.AuthorId = authorId;
			this.ApplicationId = applicationId;
			this.ApplicationName = applicationName;
			this.LoweredApplicationName = loweredApplicationName;
            this.AuthorType = authorType;
            this.Avatar = avatar;
            this.AuthorTypes = authorTypes;
            this.UseGravatar = useGravatar;
            this.ComicCount = comicCount;
			this.Comics = comics;
            this.Books = books;
            this.FullName = fullName;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
