﻿using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoAuthorType : DomainEntity<int>
    {
        public int TypeId { get; set; }
        public string Description { get; set; }

        public dtoAuthorType()
        {
        }

        public dtoAuthorType(int typeId, string description)
        {
            this.TypeId = typeId;
            this.Description = description;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
