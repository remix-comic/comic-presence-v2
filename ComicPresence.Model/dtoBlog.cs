﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public class dtoBlog : DomainEntity<int>
    {
        public bool Active { get; set; }
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string BlogSlug { get; set; }
        public string BlogTitle { get; set; }         
        public int Id { get; set; }
        public virtual ICollection<dtoPost> Posts { get; set; }
        public int PostCount { get; set; }

        public dtoBlog()
        {
            this.Posts = new List<dtoPost>();
        }

        public dtoBlog(bool active, int authorId, string authorName, string blogSlug, string blogTitle, int id, List<dtoPost> posts, int postCount)
        {
            this.Active = active;
            this.AuthorId = authorId;
            this.AuthorName = authorName;
            this.BlogSlug = blogSlug;
            this.BlogTitle = blogTitle;
            this.Id = id;
            this.Posts = posts;
            this.PostCount = postCount;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
