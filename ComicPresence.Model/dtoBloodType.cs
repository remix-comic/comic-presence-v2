﻿using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public class dtoBloodType : DomainEntity<int>
    {
        public string Type { get; set; }

        public dtoBloodType()
        {
        }

        public dtoBloodType(string type)
        {
            this.Type = type;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
