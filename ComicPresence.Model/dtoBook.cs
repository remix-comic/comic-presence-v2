using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoBook : DomainEntity<int>
    {
        public int BookId { get; set; }
        public int AuthorId { get; set; }
        public string BookTitle { get; set; }
        public string BookSlug { get; set; }
        public virtual ICollection<dtoChapter> Chapters { get; set; }

        public dtoBook()
        {
            this.Chapters = new List<dtoChapter>();
        }

        public dtoBook(int bookId, int authorId, string bookTitle, string bookSlug, List<dtoChapter> chapters)
        {
			this.BookId = bookId;
            this.AuthorId = authorId;
			this.BookTitle = bookTitle;
            this.BookSlug = bookSlug;
			this.Chapters = chapters;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.BookTitle))
                yield return new ValidationResult("The book title cannot be null or empty", new[] { "BookTitle" });

            if (this.AuthorId <= 0)
                yield return new ValidationResult("Please assign an author to this book", new[] { "AuthorId" });
        }
    }
}
