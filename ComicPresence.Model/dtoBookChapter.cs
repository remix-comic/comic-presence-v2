using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoBookChapter : DomainEntity<int>
    {
        public int RowID { get; set; }
        public int BookID { get; set; }
        public int ChapterID { get; set; }

        public dtoBookChapter()
        {

        }

        public dtoBookChapter(int rowID, int bookID, int chapterID)
        {
			this.RowID = rowID;
			this.BookID = bookID;
			this.ChapterID = chapterID;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
