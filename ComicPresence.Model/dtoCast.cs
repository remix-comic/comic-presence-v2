using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoCast : DomainEntity<int>
    {
        public int CastID { get; set; }
        public string Bio { get; set; }
        public Nullable<int> Bust { get; set; }
        public Nullable<int> Height { get; set; }
        public Nullable<int> Hips { get; set; }
        public Nullable<int> Waist { get; set; }
        public Nullable<int> Weight { get; set; }
        public string Sex { get; set; }
        public string BloodType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CastImage { get; set; }
        public string ThumbnailImage { get; set; }
        public string CastSlug { get; set; }
        public ICollection<dtoComic> Apperances { get; set; }

        public dtoCast()
        {
            this.Apperances = new List<dtoComic>();
        }

        public dtoCast(int castID, string bio, Nullable<int> bust, Nullable<int> height, Nullable<int> hips, Nullable<int> weight, Nullable<int> waist, string sex, string bloodType, string firstName, string lastName, string castImage, string thumbnailImage, string castSlug, List<dtoComic> apperances)
        {
			this.CastID = castID;
			this.Bio = bio;
            this.Bust = bust;
			this.Height = height;
            this.Hips = hips;
			this.Weight = weight;
            this.Waist = waist;
            this.Sex = sex;
			this.BloodType = bloodType;
			this.FirstName = firstName;
			this.LastName = lastName;
            this.CastImage = castImage;
            this.ThumbnailImage = thumbnailImage;
            this.CastSlug = castSlug;
            this.Apperances = apperances;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
