using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoCategory : DomainEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UrlSlug { get; set; }
        public string Description { get; set; }
        public int Used { get; set; }
        public ICollection<dtoPost> Posts { get; set; }

        public dtoCategory()
        {
            this.Posts = new List<dtoPost>();
        }

        public dtoCategory(int id, string name, string urlSlug, string description, int used, List<dtoPost> posts)
        {
			this.Id = id;
			this.Name = name;
			this.UrlSlug = urlSlug;
			this.Description = description;
            this.Used = used;
			this.Posts = posts;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.Name))
                yield return new ValidationResult("The category name cannot be null or empty.", new[] { "Name" });

            if (string.IsNullOrEmpty(this.Description))
                yield return new ValidationResult("The category description cannot be null or empty.", new[] { "Description" });
        }
    }
}
