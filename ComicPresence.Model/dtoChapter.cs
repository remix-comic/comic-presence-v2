using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoChapter : DomainEntity<int>
    {
        public int ChapterID { get; set; }
        public int ChapterNumber { get; set; }
        public string ChapterTitle { get; set; }
        public string ChapterSlug { get; set; }
        public int BookId { get; set; }
        public List<dtoComic> Comics { get; set; }

        public dtoChapter()
        {
        }

        public dtoChapter(int chapterID, int chapterNumber, string chapterTitle, string chapterSlug, int bookId, List<dtoComic> comics)
        {
			this.ChapterID = chapterID;
            this.ChapterNumber = chapterNumber;
			this.ChapterTitle = chapterTitle;
            this.ChapterSlug = chapterSlug;
            this.BookId = bookId;
			this.Comics = comics;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.ChapterTitle))
                yield return new ValidationResult("Chapter title cannot be null or empty", new[] { "ChapterTitle" });
        }
    }
}
