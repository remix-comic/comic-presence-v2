using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoComic : DomainEntity<int>
    {
        public int ComicID { get; set; }
        public int AuthorID { get; set; }
        public int IllustratorID { get; set; }
        public int WriterID { get; set; }
        public int ChapterID { get; set; }
        public int BookID { get; set; }
        public string Title { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        public string ComicImage { get; set; }
        public string ThumbImage { get; set; }
        public string BannerImage { get; set; }
        public Boolean IsArchived { get; set; }
        public Boolean IsLastComic { get; set; }
        public Boolean OnHomePage { get; set; }
        public DateTime ComicDate { get; set; }
        public TimeSpan PublishTime { get; set; }
        public dtoAuthor Author { get; set; }
        public string WriterName { get; set; }
        public string IllustratorName { get; set; }
        public dtoChapter Chapter { get; set; }
        public string ComicSlug { get; set; }
        public dtoBook Book { get; set; }
        public virtual ICollection<dtoCast> CastMembers { get; set; }
        public dtoNews News { get; set; }
        public dtoTranscript Transcript { get; set; }
        public virtual ICollection<dtoComicTag> ComicTags { get; set; }

        public dtoComic()
        {
            this.Author = new dtoAuthor();
            this.Book = new dtoBook();            
            this.CastMembers = new List<dtoCast>();
            this.Chapter = new dtoChapter();
            this.ComicTags = new List<dtoComicTag>();
            this.News = new dtoNews();
            this.Transcript = new dtoTranscript();
        }

        public dtoComic(int comicID, int authorID, int illustratorID, int writerID, int chapterID, int bookID, string title, string longDescription, string shortDescription, string comicImage, string thumbImage, string bannerImage, Boolean isArchived, Boolean isLastComic, Boolean onHomePage, DateTime comicDate, TimeSpan publishTime, dtoAuthor author, dtoChapter chapter, string comicSlug, dtoBook book, List<dtoCast> castMembers, dtoNews news, dtoTranscript transcript, List<dtoComicTag> comicTags)
        {
			this.ComicID = comicID;
			this.AuthorID = authorID;
            this.IllustratorID = illustratorID;
            this.WriterID = writerID;
			this.ChapterID = chapterID;
			this.BookID = bookID;
			this.Title = title;
			this.LongDescription = longDescription;
			this.ShortDescription = shortDescription;
			this.ComicImage = comicImage;
			this.ThumbImage = thumbImage;
			this.BannerImage = bannerImage;
			this.IsArchived = isArchived;
			this.IsLastComic = isLastComic;
			this.OnHomePage = onHomePage;
			this.ComicDate = comicDate;
            this.PublishTime = publishTime;
			this.Author = author;
			this.Chapter = chapter;
            this.ComicSlug = comicSlug;
            this.Book = book;
            this.CastMembers = castMembers;
			this.News = news;
			this.Transcript = transcript;
            this.ComicTags = comicTags;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
