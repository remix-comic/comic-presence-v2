using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoComicCast : DomainEntity<int>
    {
        public int RowID { get; set; }
        public int CastID { get; set; }
        public int ComicID { get; set; }
        public dtoCast Cast { get; set; }
        public dtoComic Comic { get; set; }

        public dtoComicCast()
        {
            this.Cast = new dtoCast();
            this.Comic = new dtoComic();
        }

        public dtoComicCast(int rowID, int castID, int comicID, dtoCast cast, dtoComic comic)
        {
			this.RowID = rowID;
			this.CastID = castID;
			this.ComicID = comicID;
			this.Cast = cast;
			this.Comic = comic;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
