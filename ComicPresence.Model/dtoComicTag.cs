using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoComicTag : DomainEntity<int>
    {
        public int RowId { get; set; }
        public int ComicId { get; set; }
        public int TagId { get; set; }

        public dtoComicTag()
        {
        }

        public dtoComicTag(int rowId, int comicId, int tagId)
        {
			this.RowId = rowId;
			this.ComicId = comicId;
			this.TagId = tagId;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
