﻿using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoConfiguration : DomainEntity<int>
    {
        public int AdminAvatarSize { get; set; }
        public string Characters { get; set; }
        public string CharacterThumbs { get; set; }
        public string ComicBanner { get; set; }
        public string ComicPath { get; set; }
        public string ComicThumbNailPath { get; set; }
        public string DefaultPostTime { get; set; }
        public string LinkImages { get; set; }
        public string SiteName { get; set; }
        public string SiteSlogan { get; set; }
        public string SiteTheme { get; set; }
        public string SiteVersion { get; set; }
        public string UserAvatar { get; set; }

        public dtoConfiguration()
        {            
        }

        public dtoConfiguration(int adminAvatarSize, string characters, string characterThumbs, string comicBanner, string comicPath, string comicThumbPathNailPath, string defaultPostTime, string linkImages, string siteName, string siteSlogan, string siteTheme, string siteVersion, string userAvatar)
        {
            this.AdminAvatarSize = adminAvatarSize;
            this.Characters = characters;
            this.CharacterThumbs = characterThumbs;
            this.ComicBanner = comicBanner;
            this.ComicPath = comicPath;
            this.ComicThumbNailPath = comicThumbPathNailPath;
            this.DefaultPostTime = defaultPostTime;
            this.LinkImages = linkImages;
            this.SiteName = siteName;
            this.SiteSlogan = siteSlogan;
            this.SiteTheme = siteTheme;
            this.SiteVersion = siteVersion;
            this.UserAvatar = userAvatar;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
