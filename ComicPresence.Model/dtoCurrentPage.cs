﻿using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoCurrentPage : DomainEntity<int>
    {
        public string ComicSlug { get; set; }
        public string ComicTitle { get; set; }
        public int FirstPage { get; set; }
        public string ImageUrl { get; set; }

        public dtoCurrentPage()
        {
        }

        public dtoCurrentPage(string comicSlug, string comicTitle, int firstPage, string imageUrl)
        {
            this.ComicSlug = comicSlug;
            this.ComicTitle = comicTitle;
            this.FirstPage = firstPage;
            this.ImageUrl = imageUrl;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
