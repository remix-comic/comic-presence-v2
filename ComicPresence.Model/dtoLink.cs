using System;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoLink : DomainEntity<int>
    {
        public int LinkId { get; set; }
        public string AlternateName { get; set; }
        public string ExternalUrl { get; set; }
        public string ImagePath { get; set; }
        public Boolean IsActive { get; set; }

        public dtoLink()
        {
        }

        public dtoLink(int linkId, string alternateName, string externalUrl, string imagePath, Boolean isActive)
        {
			this.LinkId = linkId;
			this.AlternateName = alternateName;
			this.ExternalUrl = externalUrl;
			this.ImagePath = imagePath;
			this.IsActive = isActive;
        }

        public override System.Collections.Generic.IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.AlternateName))
                yield return new ValidationResult("The Name field cannot be null or empty.", new[] { "AlternateName" });

            if (string.IsNullOrEmpty(this.ExternalUrl))
                yield return new ValidationResult("The hyper-link field cannot be null or empty.", new[] { "ExternalUrl " });
        }
    }
}
