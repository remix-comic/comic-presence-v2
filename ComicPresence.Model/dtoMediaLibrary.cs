﻿using System;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoMediaLibrary : DomainEntity<int>
    {
        public string Author { get; set; }
        public int MediaId { get; set; }
        public int MediaType { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string ImagePath { get; set; }
        public string ThumbPath { get; set; }
        public string Extention { get; set; }
        public string Dimensions { get; set; }
        public DateTime Uploaded { get; set; }
        public string GalleryName { get; set; }

        public dtoMediaLibrary()
        {            
        }

        public dtoMediaLibrary(string author, int mediaId, int mediaType, string name, string slug, string imagePath, string thumbPath, string extention, string dimensions, DateTime uploaded, string galleryName)
        {
            this.Author = author;
            this.MediaId = mediaId;
            this.MediaType = mediaType;
            this.Name = name;
            this.Slug = slug;
            this.ImagePath = imagePath;
            this.ThumbPath = thumbPath;
            this.Extention = extention;
            this.Dimensions = dimensions;
            this.Uploaded = uploaded;
            this.GalleryName = galleryName;
        }

        public override System.Collections.Generic.IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.Name))
                yield return new ValidationResult("The media name cannot be empty or null", new[] { "Name" });

            if (string.IsNullOrEmpty(this.ImagePath))
                yield return new ValidationResult("Please select and image", new[] { "ImagePath" });

            if (string.IsNullOrEmpty(this.ThumbPath))
                yield return new ValidationResult("Please select a thumb nail image", new[] { "ThumPath" });
        }
    }
}
