﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoMediaType : DomainEntity<int>
    {
        public int TypeId { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public virtual ICollection<dtoMediaLibrary> MediaLibraries { get; set; }

        public dtoMediaType()
        {            
            this.MediaLibraries = new List<dtoMediaLibrary>();
        }

        public dtoMediaType(int typeId, string description, string slug, List<dtoMediaLibrary> mediaLibraries)
        {
            this.TypeId = typeId;
            this.Description = description;
            this.Slug = slug;
            this.MediaLibraries = mediaLibraries;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.Description))
                yield return new ValidationResult("A description for the new tag is required", new[] { "Description " });            
        }
    }
}
