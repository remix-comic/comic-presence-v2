﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public class dtoMenu : DomainEntity<int>
    {
        public virtual ICollection<dtoaspnet_Membership> Authors { get; set; }
        public virtual ICollection<dtoNews> Articles { get; set; }
        public virtual ICollection<dtoBlog> Blogs { get; set; }
        public virtual ICollection<dtoBook> Books { get; set; }
        public virtual ICollection<dtoCast> CastMembers { get; set; }
        public virtual ICollection<dtoCategory> Categories { get; set; }
        public virtual ICollection<dtoChapter> Chapters { get; set; }
        public virtual ICollection<dtoComic> Comics { get; set; }
        public virtual ICollection<dtoLink> Links { get; set; }
        public virtual ICollection<dtoaspnet_Roles> Roles { get; set; }
        public virtual ICollection<dtoTag> Tags { get; set; }
        public virtual ICollection<dtoTranscript> Transcripts { get; set; }

        public dtoMenu()
        {
            this.Authors = new List<dtoaspnet_Membership>();
            this.Articles = new List<dtoNews>();
            this.Blogs = new List<dtoBlog>();
            this.Books = new List<dtoBook>();
            this.CastMembers = new List<dtoCast>();
            this.Chapters = new List<dtoChapter>();
            this.Comics = new List<dtoComic>();
            this.Links = new List<dtoLink>();
            this.Roles = new List<dtoaspnet_Roles>();
            this.Tags = new List<dtoTag>();
            this.Transcripts = new List<dtoTranscript>();
        }
        
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
