﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public class dtoNewAccount : DomainEntity<int>
    {
        [Required()]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required()]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required()]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required()]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }

        [Required()]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Author Roles")]
        public Dictionary<string, string> Roles { get; set; }
       
        [Display(Name = "Author Types")]
        public virtual ICollection<dtoAuthorType> AuthorTypes { get; set; }

        public dtoNewAccount()
        {
            this.AuthorTypes = new List<dtoAuthorType>();
            this.Roles = new Dictionary<string,string>();
        }

        public dtoNewAccount(string firstName, string lastName, string userName, string email, string password, Dictionary<string, string> roles)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.UserName = userName;
            this.Email = email;
            this.Password = password;
            this.Roles = roles;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.FirstName))
                yield return new ValidationResult("First name cannot be null or empty", new[] { "FirstName" });

            if (string.IsNullOrEmpty(this.LastName))
                yield return new ValidationResult("Last name cannot be null or empty", new[] { "LastName" });

            if (string.IsNullOrEmpty(this.Email))
                yield return new ValidationResult("Email cannot be null or empty", new[] { "Email" });

            if (string.IsNullOrEmpty(this.UserName))
                yield return new ValidationResult("User name cannot be null or empty", new[] { "UserName" });

            if (string.IsNullOrEmpty(this.Password))
                yield return new ValidationResult("A password is required", new[] { "Password" });
        }
    }
}
