﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public class dtoNewCharacter : DomainEntity<int>
    {
        public string Bio { get; set; }

        [Display(Name = "Blood Type")]
        public string BloodType { get; set; }
        public string Bust { get; set; }

        [Required()]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public string Height { get; set; }
        public string Hips { get; set; }

        [Required()]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string Slug { get; set; }
        public string Waist { get; set; }
        public string Weight { get; set; }
        public ICollection<dtoBloodType> BloodTypes { get; set; }
        public string Updated { get; set; }
        public string Success { get; set; }

        public dtoNewCharacter()
        {
            this.BloodTypes = new List<dtoBloodType>();
        }

        public dtoNewCharacter(string bio, string bloodType, List<dtoBloodType> bloodTypes, string bust, string firstName, string gender, string height, string hips, string lastName, string slug, string waist, string weight, string updated, string success)
        {
            this.Bio = bio;
            this.BloodType = bloodType;
            this.BloodTypes = bloodTypes;
            this.Bust = bust;
            this.FirstName = firstName;
            this.Gender = gender;
            this.Height = height;
            this.Hips = hips;
            this.LastName = lastName;
            this.Slug = slug;
            this.Waist = waist;
            this.Weight = weight;
            this.Updated = updated;
            this.Success = success;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.FirstName))
                yield return new ValidationResult("The first name cannot be null or empty", new[] { "FirstName" });

            if (string.IsNullOrEmpty(this.LastName))
                yield return new ValidationResult("The last name cannot be null or empty", new[] { "LastName" });
        }
    }
}
