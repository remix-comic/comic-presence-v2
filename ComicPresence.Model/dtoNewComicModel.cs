﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoNewComicModel : DomainEntity<int>
    {
        public int AuthorId { get; set; }
        public int IllustratorId { get; set; }
        public int WriterId { get; set; }
        public int ChapterId { get; set; }
        public int BookId { get; set; }
        public bool IsArchived { get; set; }
        public bool IsLastComic { get; set; }
        public bool OnHomePage { get; set; }
        public DateTime ComicDate { get; set; }
        public TimeSpan PublishTime { get; set; }
        public string ComicSlug { get; set; }
        public string Transcript { get; set; }

        [Required()]
        [Display(Name = "Comic Title")]
        public string Title { get; set; }

        [Display(Name = "Banner Image")]
        public string BannerImage { get; set; }

        [Required()]
        [Display(Name = "Comic Image")]
        public string ComicImage { get; set; }

        [Required()]
        [Display(Name = "Thumbnail Image")]
        public string ThumbnailImage { get; set; }

        [Required()]
        [Display(Name = "Long Description")]
        public string LongDescription { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }

        public virtual ICollection<dtoTag> Tags { get; set; }
        public virtual ICollection<dtoChapter> Chapters { get; set; }
        public virtual ICollection<dtoBook> Books { get; set; }
        public virtual ICollection<dtoaspnet_Membership> Authors { get; set; }
        public virtual ICollection<dtoCast> CastMembers { get; set; }

        public dtoNewComicModel()
        {
            this.Authors = new List<dtoaspnet_Membership>();
            this.Books = new List<dtoBook>();
            this.CastMembers = new List<dtoCast>();
            this.Chapters = new List<dtoChapter>();
            this.Tags = new List<dtoTag>();
        }

        public dtoNewComicModel(int authorId, int illustratorId, int writerId, int chapterId, int bookId, Boolean isArchived, Boolean isLastComic, Boolean onHomePage, DateTime comicDate, TimeSpan publishTime, string comicSlug, string transcript, string title, string bannerImage, string comicImage, string thumbnailImage, string longDescription, string shortDescription, List<dtoTag> tags, List<dtoChapter> chapters, List<dtoBook> books, List<dtoaspnet_Membership> authors, List<dtoCast> castMembers)
        {
            this.AuthorId = authorId;
            this.Authors = authors;
            this.BannerImage = bannerImage;
            this.BookId = bookId;
            this.Books = books;
            this.CastMembers = castMembers;
            this.ChapterId = chapterId;
            this.Chapters = chapters;
            this.ComicDate = comicDate;
            this.ComicImage = comicImage;
            this.ComicSlug = comicSlug;
            this.IllustratorId = illustratorId;
            this.IsArchived = isArchived;
            this.IsLastComic = isLastComic;
            this.LongDescription = longDescription;
            this.OnHomePage = onHomePage;
            this.PublishTime = publishTime;
            this.ShortDescription = shortDescription;
            this.Tags = tags;
            this.ThumbnailImage = thumbnailImage;
            this.Title = title;
            this.Transcript = transcript;
            this.WriterId = writerId;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.ComicImage))
                yield return new ValidationResult("A new comic image cannot be found please select an image.", new[] { "ComicImage" });

            if (string.IsNullOrEmpty(this.LongDescription))
                yield return new ValidationResult("The long comic description cannot be null or empty.", new[] { "Long Description" });

            if (string.IsNullOrEmpty(this.ThumbnailImage))
                yield return new ValidationResult("A new thumbnail image cannot be found please select an image.", new[] { "ThumbnailImage" });

            if (string.IsNullOrEmpty(this.Title))
                yield return new ValidationResult("The title cannot be null or empty", new[] { "Title" });
        }
    }    
}
