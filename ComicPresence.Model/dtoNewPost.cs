﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoNewPost : DomainEntity<int>
    {
        [Display(Name = "Title")]
        [Required()]
        public string Title { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }

        [AllowHtml]
        [Display(Name = "Long Description")]
        [Required()]
        public string LongDescription { get; set; }

        [Display(Name = "Meta Description")]
        public string Meta { get; set; }

        public int Category { get; set; }

        public Dictionary<int, int> Tags { get; set; }

        public string Status { get; set; }

        public string Visibility { get; set; }

        public string Publish { get; set; }

        public dtoNewPost()
        {
            this.Tags = new Dictionary<int, int>();
        }

        public dtoNewPost(string title, string longDescription, string meta, int category, Dictionary<int, int> tags, string status, string visibility, string publish)
        {
            this.Title = title;
            this.LongDescription = longDescription;
            this.Meta = meta;
            this.Category = category;
            this.Tags = tags;
            this.Status = status;
            this.Visibility = visibility;
            this.Publish = publish;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.LongDescription))
                yield return new ValidationResult("The post content cannot be null or empty", new[] { "LongDescritption" });

            if (string.IsNullOrEmpty(this.Title))
                yield return new ValidationResult("The post title cannot be null or empty", new[] { "Title" });
        }
    }
}
