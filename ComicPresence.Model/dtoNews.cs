using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoNews : DomainEntity<int>
    {
        public int NewsId { get; set; }
        public int ComicId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime NewsDate { get; set; }
        public string NewsSlug { get; set; }
        public dtoComic Comic { get; set; }
        public virtual ICollection<dtoComic> Comics { get; set; }

        public dtoNews()
        {
        }

        public dtoNews(int newsId, int comicId, string title, string text, DateTime newsDate, string newsSlug, dtoComic comic, List<dtoComic> comics)
        {
			this.NewsId = newsId;
			this.ComicId = comicId;
			this.Title = title;
			this.Text = text;
			this.NewsDate = newsDate;
            this.NewsSlug = newsSlug;
			this.Comic = comic;
            this.Comics = comics;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.Title))
                yield return new ValidationResult("The news title cannot be null or empty", new [] { "Title" });

            if (string.IsNullOrEmpty(this.Text))
                yield return new ValidationResult("The content of the news article cannot be null of empty", new[] { "Text" });
        }
    }
}
