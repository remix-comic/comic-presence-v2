using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoPost : DomainEntity<int>
    {
        public int Id { get; set; }
        public int BlogId { get; set; }
        public string Title { get; set; }

        [AllowHtml]
        public string ShortDescription { get; set; }

        [AllowHtml]
        public string LongDescription { get; set; }
        public string Meta { get; set; }
        public string UrlSlug { get; set; }
        public Nullable<bool> Published { get; set; }
        public Nullable<DateTime> PostedOn { get; set; }
        public string Modified { get; set; }
        public int CategoryId { get; set; }
        public dtoCategory Category { get; set; }
        public string Status { get; set; }
        public virtual ICollection<dtoTag> Tags { get; set; }
        public string Visibility { get; set; }
        public string BlogName { get; set; }
        public string BlogSlug { get; set; }

        public dtoPost()
        {
            this.Category = new dtoCategory();
            this.Tags = new List<dtoTag>();
        }

        public dtoPost(int id, int blogId, string title, string shortDescription, string longDescription, string meta, string urlSlug, Nullable<bool> published, Nullable<DateTime> postedOn, string modified, int categoryId, dtoCategory category, string status, List<dtoTag> tags, string visibility, string blogName, string blogSlug)
        {
			this.Id = id;
            this.BlogId = blogId;
			this.Title = title;
			this.ShortDescription = shortDescription;
			this.LongDescription = longDescription;
			this.Meta = meta;
			this.UrlSlug = urlSlug;
			this.Published = published;
			this.PostedOn = postedOn;
			this.Modified = modified;
			this.CategoryId = categoryId;
			this.Category = category;
            this.Status = status;
			this.Tags = tags;
            this.Visibility = visibility;
            this.BlogName = blogName;
            this.BlogSlug = blogSlug;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.LongDescription))
                yield return new ValidationResult("The post content cannot be null or empty", new[] { "LongDescription" });

            if (string.IsNullOrEmpty(this.Title))
                yield return new ValidationResult("The post title cannot be null or empty", new[] { "Title" });
        }
    }
}
