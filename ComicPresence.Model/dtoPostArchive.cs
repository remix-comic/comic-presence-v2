﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoPostArchive : DomainEntity<int>
    {
        public string Month { get; set; }
        public int Year { get; set; }
        public virtual ICollection<dtoPost> Posts { get; set; }

        public dtoPostArchive()
        {
            this.Posts = new List<dtoPost>();
        }

        public dtoPostArchive(string month, int year, List<dtoPost> posts)
        {
            this.Month = month;
            this.Year = year;
            this.Posts = posts;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
