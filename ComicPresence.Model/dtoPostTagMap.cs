using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoPostTagMap : DomainEntity<int>
    {
        public int RowId { get; set; }
        public int PostId { get; set; }
        public int TagId { get; set; }
        public dtoPost Post { get; set; }

        public dtoPostTagMap()
        {
            this.Post = new dtoPost();
        }

        public dtoPostTagMap(int rowId, int postId, int tagId, dtoPost post)
        {
			this.RowId = rowId;
			this.PostId = postId;
			this.TagId = tagId;
			this.Post = post;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
