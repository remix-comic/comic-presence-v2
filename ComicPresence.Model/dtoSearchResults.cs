﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoSearchResults : DomainEntity<int>
    {
        public virtual ICollection<dtoPost> BlogResults { get; set; }
        public virtual ICollection<dtoComic> ComicResults { get; set; }
        public virtual ICollection<dtoMediaLibrary> MediaResults { get; set; }

        public dtoSearchResults()
        {
            this.BlogResults = new List<dtoPost>();
            this.ComicResults = new List<dtoComic>();
            this.MediaResults = new List<dtoMediaLibrary>();
        }

        public dtoSearchResults(List<dtoPost> blogResults, List<dtoComic> comicResults, List<dtoMediaLibrary> mediaResults)
        {
            this.BlogResults = blogResults;
            this.ComicResults = comicResults;
            this.MediaResults = mediaResults;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
