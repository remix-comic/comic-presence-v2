using System;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoSiteSetting : DomainEntity<int>
    {
        public string ConfigurationKey { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }

        public dtoSiteSetting()
        {
        }

        public dtoSiteSetting(string configurationKey, string description, string value)
        {
			this.ConfigurationKey = configurationKey;
			this.Description = description;
			this.Value = value;
        }

        public override System.Collections.Generic.IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.Description))
                yield return new ValidationResult("The configuration key description cannot be null or empty", new[] { "Description" });

            if (string.IsNullOrEmpty(this.Value))
                yield return new ValidationResult("The configuration key value cannot be null or empty", new[] { "Key" });
        }
    }
}
