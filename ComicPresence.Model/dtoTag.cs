using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoTag : DomainEntity<int>
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
        public string TagSlug { get; set; }
        public int TagCount { get; set; }
        public virtual ICollection<dtoPost> Posts { get; set; }
        public bool Used { get; set; }
        public string FontSize { get; set; }

        public dtoTag()
        {
            this.Posts = new List<dtoPost>();
        }

        public dtoTag(string fontSize, int tagId, string tagName, string tagSlug, int tagCount, List<dtoPost> posts, bool used)
        {
            this.FontSize = fontSize;
			this.TagId = tagId;
			this.TagName = tagName;
            this.TagSlug = tagSlug;
            this.TagCount = tagCount;
            this.Posts = posts;
            this.Used = used;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.TagName))
                yield return new ValidationResult("The name of the new tag cannot be null or empty", new[] { "TagName" });
        }
    }
}
