using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoTranscript : DomainEntity<int>
    {
        public int TranscriptId { get; set; }
        public string ScriptText { get; set; }
        public int ParentComic { get; set; }
        public int AuthorId { get; set; }
        public string Slug { get; set; }
        public dtoComic Comic { get; set; }

        public dtoTranscript()
        {
        }

        public dtoTranscript(int transcriptId, string scriptText, int parentComic, int authorId, string slug, dtoComic comic)
        {
			this.TranscriptId = transcriptId;
			this.ScriptText = scriptText;
			this.ParentComic = parentComic;
			this.AuthorId = authorId;
            this.Slug = slug;
			this.Comic = comic;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
