using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoaspnet_Applications : DomainEntity<int>
    {
        public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        public Guid ApplicationId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<dtoaspnet_Membership> Aspnet_Membership { get; set; }
        public virtual ICollection<dtoaspnet_Roles> Aspnet_Roles { get; set; }
        public virtual ICollection<dtoaspnet_Users> Aspnet_Users { get; set; }

        public dtoaspnet_Applications()
        {
            this.Aspnet_Membership = new List<dtoaspnet_Membership>();
            this.Aspnet_Roles = new List<dtoaspnet_Roles>();
            this.Aspnet_Users = new List<dtoaspnet_Users>();
        }

        public dtoaspnet_Applications(string applicationName, string loweredApplicationName, Guid applicationId, string description, List<dtoaspnet_Membership> aspnet_Membership, List<dtoaspnet_Roles> aspnet_Roles, List<dtoaspnet_Users> aspnet_Users)
        {
			this.ApplicationName = applicationName;
			this.LoweredApplicationName = loweredApplicationName;
			this.ApplicationId = applicationId;
			this.Description = description;
            this.Aspnet_Membership = aspnet_Membership;
			this.Aspnet_Roles = aspnet_Roles;
            this.Aspnet_Users = aspnet_Users;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
