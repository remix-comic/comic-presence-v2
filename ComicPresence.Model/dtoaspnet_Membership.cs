using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoaspnet_Membership : DomainEntity<int>
    {
        public System.Guid ApplicationId { get; set; }
        public System.Guid UserId { get; set; }
        public int AuthorId { get; set; }
        public string Password { get; set; }
        public int PasswordFormat { get; set; }
        public string PasswordSalt { get; set; }
        public string MobilePIN { get; set; }
        public string Email { get; set; }
        public string LoweredEmail { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }        
        public string Comment { get; set; }
        public string Bio { get; set; }
        public dtoAuthor Author { get; set; }
        public dtoaspnet_Users Aspnet_Users { get; set; }
        public virtual ICollection<dtoaspnet_Roles> Aspnet_Roles { get; set; }

        public dtoaspnet_Membership()
        {
            this.Aspnet_Roles = new List<dtoaspnet_Roles>();
            this.Aspnet_Users = new dtoaspnet_Users();
            this.Author = new dtoAuthor();
        }

        public dtoaspnet_Membership(Guid applicationId, Guid userId, int authorId, string password, int passwordFormat, string passwordSalt, string mobilePIN, string email, string loweredEmail, string passwordQuestion, string passwordAnswer, string firstName, string lastName, Boolean isApproved, Boolean isLockedOut,  string comment, string bio, dtoAuthor author, dtoaspnet_Users aspnet_Users, List<dtoaspnet_Roles> aspnet_roles)
        {
			this.ApplicationId = applicationId;
			this.UserId = userId;
			this.AuthorId = authorId;
			this.Password = password;
			this.PasswordFormat = passwordFormat;
			this.PasswordSalt = passwordSalt;
			this.MobilePIN = mobilePIN;
			this.Email = email;
			this.LoweredEmail = loweredEmail;
			this.PasswordQuestion = passwordQuestion;
			this.PasswordAnswer = passwordAnswer;
			this.FirstName = firstName;
			this.LastName = lastName;
			this.IsApproved = isApproved;
			this.IsLockedOut = isLockedOut;
			this.Comment = comment;
			this.Bio = bio;
            this.Author = author;
			this.Aspnet_Users = aspnet_Users;
            this.Aspnet_Roles = aspnet_roles;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
