using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoaspnet_Profile : DomainEntity<int>
    {
        public Guid UserId { get; set; }
        public string PropertyNames { get; set; }
        public string PropertyValuesstring { get; set; }
        public byte[] PropertyValuesBinary { get; set; }
        public DateTime LastUpdatedDate { get; set; }

        public dtoaspnet_Profile()
        {
        }

        public dtoaspnet_Profile(Guid userId, string propertyNames, string propertyValuesstring, Byte[] propertyValuesBinary, DateTime lastUpdatedDate)
        {
			this.UserId = userId;
			this.PropertyNames = propertyNames;
			this.PropertyValuesstring = propertyValuesstring;
			this.PropertyValuesBinary = propertyValuesBinary;
			this.LastUpdatedDate = lastUpdatedDate;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
