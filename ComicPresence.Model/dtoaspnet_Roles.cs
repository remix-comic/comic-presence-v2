using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoaspnet_Roles :  DomainEntity<int>
    {
        public Guid ApplicationId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
        public dtoaspnet_Applications aspnet_Applications { get; set; }
        public virtual ICollection<dtoaspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }

        public dtoaspnet_Roles()
        {
            this.aspnet_Applications = new dtoaspnet_Applications();
            this.aspnet_UsersInRoles = new List<dtoaspnet_UsersInRoles>();
        }

        public dtoaspnet_Roles(Guid applicationId, Guid roleId, string roleName, string loweredRoleName, string description, dtoaspnet_Applications aspnet_Applications, List<dtoaspnet_UsersInRoles> aspnet_UsersInRoles)
        {
			this.ApplicationId = applicationId;
			this.RoleId = roleId;
			this.RoleName = roleName;
			this.LoweredRoleName = loweredRoleName;
			this.Description = description;
			this.aspnet_Applications = aspnet_Applications;
			this.aspnet_UsersInRoles = aspnet_UsersInRoles;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.RoleName))
                yield return new ValidationResult("Role name cannot be null or empty", new[] { "RoleName" });

            if (string.IsNullOrEmpty(this.Description))
                yield return new ValidationResult("Description cannot be null or empty", new[] { "Description" });
        }
    }
}
