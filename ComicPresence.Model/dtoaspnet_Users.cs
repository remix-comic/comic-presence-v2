using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoaspnet_Users : DomainEntity<int>
    {
        public Guid ApplicationId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string LoweredUserName { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public DateTime LastActivityDate { get; set; }
        public dtoaspnet_Membership aspnet_Membership { get; set; }
        public virtual ICollection<dtoaspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }

        public dtoaspnet_Users()
        {
            this.aspnet_UsersInRoles = new List<dtoaspnet_UsersInRoles>();
        }

        public dtoaspnet_Users(Guid applicationId, Guid userId, string userName, string loweredUserName, string mobileAlias, Boolean isAnonymous, DateTime lastActivityDate, dtoaspnet_Membership aspnet_Membership, IList<dtoaspnet_UsersInRoles> aspnet_UsersInRoles)
        {
			this.ApplicationId = applicationId;
			this.UserId = userId;
			this.UserName = userName;
			this.LoweredUserName = loweredUserName;
			this.MobileAlias = mobileAlias;
			this.IsAnonymous = isAnonymous;
			this.LastActivityDate = lastActivityDate;
			this.aspnet_Membership = aspnet_Membership;
            this.aspnet_UsersInRoles = aspnet_UsersInRoles;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
