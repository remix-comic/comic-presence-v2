using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ComicPresence.Infrastructure;

namespace ComicPresence.Model
{
    public partial class dtoaspnet_UsersInRoles : DomainEntity<int>
    {
        public int RowId { get; set; }
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public dtoaspnet_Roles aspnet_Roles { get; set; }

        public dtoaspnet_UsersInRoles()
        {
            this.aspnet_Roles = new dtoaspnet_Roles();
        }

        public dtoaspnet_UsersInRoles(int rowId, Guid userId, Guid roleId, dtoaspnet_Roles aspnet_Roles)
        {
			this.RowId = rowId;
			this.UserId = userId;
			this.RoleId = roleId;
			this.aspnet_Roles = aspnet_Roles;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }
    }
}
