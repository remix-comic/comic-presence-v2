﻿
namespace ComicPresence.Repositories.EF
{
    /// <summary>
    /// Used to initialize the CmsContext.
    /// </summary>
    public static class CmsContextInitializer
    {
        public static void Init()
        {
            using (var db = new CmsManagerContext())
            {
                db.Database.Initialize(false);
            }
        }
    }
}
