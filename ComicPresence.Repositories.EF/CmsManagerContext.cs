﻿using System;
using System.Data.Entity;
using System.Linq;

namespace ComicPresence.Repositories.EF
{
    /// <summary>
    /// This is the main DbContext to work with data in the database.
    /// </summary>
    public class CmsManagerContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the CmsManagerContext class.
        /// </summary>
        public CmsManagerContext()
            : base("CmsContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
    }
}
