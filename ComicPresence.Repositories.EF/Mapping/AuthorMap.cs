using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Author"/> and <see cref="dtoAuthor"/>.
    /// </summary>
    public static partial class AuthorMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoAuthor"/> converted from <see cref="Author"/>.</param>
        static partial void OnDTO(this Author entity, dtoAuthor dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Author"/> converted from <see cref="dtoAuthor"/>.</param>
        static partial void OnEntity(this dtoAuthor dto, Author entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoAuthor"/> to an instance of <see cref="Author"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoAuthor"/> to convert.</param>
        public static Author ToEntity(this dtoAuthor dto)
        {
            if (dto == null) return null;

            var entity = new Author();

            entity.AuthorID = dto.AuthorId;
            entity.ApplicationId = dto.ApplicationId;
            entity.ApplicationName = dto.ApplicationName;
            entity.LoweredApplicationName = dto.LoweredApplicationName;
            entity.AuthorType = dto.AuthorType;
            entity.Avatar = dto.Avatar;
            entity.UseGravatar = dto.UseGravatar;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Author"/> to an instance of <see cref="dtoAuthor"/>.
        /// </summary>
        /// <param name="entity"><see cref="Author"/> to convert.</param>
        public static dtoAuthor ToDTO(this Author entity)
        {
            if (entity == null) return null;

            var dto = new dtoAuthor();

            dto.AuthorId = entity.AuthorID;
            dto.ApplicationId = entity.ApplicationId;
            dto.ApplicationName = entity.ApplicationName;
            dto.LoweredApplicationName = entity.LoweredApplicationName;
            dto.AuthorType = entity.AuthorType;
            dto.Avatar = entity.Avatar;
            dto.UseGravatar = entity.UseGravatar;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoAuthor"/> to an instance of <see cref="Author"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Author> ToEntities(this IEnumerable<dtoAuthor> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Author"/> to an instance of <see cref="dtoAuthor"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoAuthor> ToDTOs(this IEnumerable<Author> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Author"/> to an instance of <see cref="dtoAuthor"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoAuthor> ToQueryDTOs(this IQueryable<Author> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
