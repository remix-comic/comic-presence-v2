﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{
    /// <summary>
    /// Map for <see cref=" cref="AuthorType"/> and <see cref="dtoAuthorType"/>.
    /// </summary>
    public static partial class AuthorTypeMap
    {
        /// <summary>
        ///Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"></param> converted from <see cref="AuthorType"/>.
        static partial void OnDto(this AuthorType entity, dtoAuthorType dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="dtoAuthorType"/> converted from.</param>
        static partial void OnEntity(this dtoAuthorType dto, AuthorType entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoAuthorType"/> to an instance of <see cref="AuthorType"/>
        /// </summary>
        /// <param name="dto"><see cref="dtoAuthorType"/>to convert.</param>
        /// <returns><see cref="AuthorType"/></returns>
        public static AuthorType ToEntity(this dtoAuthorType dto)
        {
            if (dto == null) return null;

            var entity = new AuthorType();

            entity.TypeId = dto.TypeId;
            entity.Description = dto.Description;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="AuthorType"/> to an instance of <see cref="dtoAuthorType"/>
        /// </summary>
        /// <param name="entity"><see cref="AuthorType"/>to convert.</param>
        /// <returns><see cref="dtoAuthorType"/></returns>
        public static dtoAuthorType ToDTO(this AuthorType entity)
        {
            if (entity == null) return null;

            var dto = new dtoAuthorType();

            dto.TypeId = entity.TypeId;
            dto.Description = entity.Description;

            entity.OnDto(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoAuthorType"/> to an instance of <see cref="AuthorType"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<AuthorType> ToEntities(this IEnumerable<dtoAuthorType> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="AuthorType"/> to an instance of <see cref="dtoAuthorType"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoAuthorType> ToDTOs(this IEnumerable<AuthorType> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="AuthorType"/> to an instance of <see cref="dtoAuthorType"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoAuthorType> ToQueryDTOs(this IQueryable<AuthorType> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
