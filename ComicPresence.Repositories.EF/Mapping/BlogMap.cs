﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{
    /// <summary>
    /// Map for the <see cref="Blog" /> and <see cref="dtoBlog"/>
    /// </summary>
    public static partial class BlogMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoBlog"/> converted from <see cref="Blog"/>.</param>
        static partial void OnDTO(this Blog entity, dtoBlog dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Blog"/> converted from <see cref="dtoBlog"/>.</param>
        static partial void OnEntity(this dtoBlog dto, Blog entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoBlog"/> to an instance of <see cref="Blog"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoBlog"/> to convert.</param>
        /// <returns><see cref="Blog"/>.</returns>
        public static Blog ToEntity(this dtoBlog dto)
        {
            if (dto == null) return null;

            var entity = new Blog();

            entity.Active = dto.Active;
            entity.AuthorId = dto.AuthorId;
            entity.BlogSlug = dto.BlogSlug;
            entity.BlogTitle = dto.BlogTitle;
            entity.Id = dto.Id;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Blog"/> to an instance of <see cref="dtoBlog"/>.
        /// </summary>
        /// <param name="entity"><see cref="Blog"/> to convert.</param>
        public static dtoBlog ToDTO(this Blog entity)
        {
            if (entity == null) return null;

            var dto = new dtoBlog();

            dto.Active = entity.Active;
            dto.AuthorId = entity.AuthorId;
            dto.BlogSlug = entity.BlogSlug;
            dto.BlogTitle = entity.BlogTitle;
            dto.Id = entity.Id;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoBlog"/> to an instance of <see cref="Blog"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Blog> ToEntities(this IEnumerable<dtoBlog> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Blog"/> to an instance of <see cref="dtoBlog"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoBlog> ToDTOs(this IEnumerable<Blog> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Blog"/> to an instance of <see cref="dtoBlog"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoBlog> ToQueryDTOs(this IQueryable<Blog> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
