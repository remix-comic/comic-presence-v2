using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="BookChapter"/> and <see cref="dtoBookChapter"/>.
    /// </summary>
    public static partial class BookChapterMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoBookChapter"/> converted from <see cref="BookChapter"/>.</param>
        static partial void OnDTO(this BookChapter entity, dtoBookChapter dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="BookChapter"/> converted from <see cref="dtoBookChapter"/>.</param>
        static partial void OnEntity(this dtoBookChapter dto, BookChapter entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoBookChapter"/> to an instance of <see cref="BookChapter"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoBookChapter"/> to convert.</param>
        public static BookChapter ToEntity(this dtoBookChapter dto)
        {
            if (dto == null) return null;

            var entity = new BookChapter();

            entity.RowID = dto.RowID;
            entity.BookID = dto.BookID;
            entity.ChapterID = dto.ChapterID;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="BookChapter"/> to an instance of <see cref="dtoBookChapter"/>.
        /// </summary>
        /// <param name="entity"><see cref="BookChapter"/> to convert.</param>
        public static dtoBookChapter ToDTO(this BookChapter entity)
        {
            if (entity == null) return null;

            var dto = new dtoBookChapter();

            dto.RowID = entity.RowID;
            dto.BookID = entity.BookID;
            dto.ChapterID = entity.ChapterID;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoBookChapter"/> to an instance of <see cref="BookChapter"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<BookChapter> ToEntities(this IEnumerable<dtoBookChapter> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="BookChapter"/> to an instance of <see cref="dtoBookChapter"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoBookChapter> ToDTOs(this IEnumerable<BookChapter> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="BookChapter"/> to an instance of <see cref="dtoBookChapter"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoBookChapter> ToQueryDTOs(this IQueryable<BookChapter> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
