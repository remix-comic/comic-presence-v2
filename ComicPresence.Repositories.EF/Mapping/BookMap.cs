using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Book"/> and <see cref="dtoBook"/>.
    /// </summary>
    public static partial class BookMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoBook"/> converted from <see cref="Book"/>.</param>
        static partial void OnDTO(this Book entity, dtoBook dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Book"/> converted from <see cref="dtoBook"/>.</param>
        static partial void OnEntity(this dtoBook dto, Book entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoBook"/> to an instance of <see cref="Book"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoBook"/> to convert.</param>
        public static Book ToEntity(this dtoBook dto)
        {
            if (dto == null) return null;

            var entity = new Book();

            entity.BookID = dto.BookId;
            entity.AuthorID = dto.AuthorId;
            entity.BookTitle = dto.BookTitle;
            entity.BookSlug = dto.BookSlug;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Book"/> to an instance of <see cref="dtoBook"/>.
        /// </summary>
        /// <param name="entity"><see cref="Book"/> to convert.</param>
        public static dtoBook ToDTO(this Book entity)
        {
            if (entity == null) return null;

            var dto = new dtoBook();

            dto.BookId= entity.BookID;
            dto.AuthorId = entity.AuthorID;
            dto.BookTitle = entity.BookTitle;
            dto.BookSlug = entity.BookSlug;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoBook"/> to an instance of <see cref="Book"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Book> ToEntities(this IEnumerable<dtoBook> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Book"/> to an instance of <see cref="dtoBook"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoBook> ToDTOs(this IEnumerable<Book> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Book"/> to an instance of <see cref="dtoBook"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoBook> ToQueryDTOs(this IQueryable<Book> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
