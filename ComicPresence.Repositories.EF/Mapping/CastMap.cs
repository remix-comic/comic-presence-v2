using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Cast"/> and <see cref="dtoCast"/>.
    /// </summary>
    public static partial class CastMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoCast"/> converted from <see cref="Cast"/>.</param>
        static partial void OnDTO(this Cast entity, dtoCast dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Cast"/> converted from <see cref="dtoCast"/>.</param>
        static partial void OnEntity(this dtoCast dto, Cast entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoCast"/> to an instance of <see cref="Cast"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoCast"/> to convert.</param>
        public static Cast ToEntity(this dtoCast dto)
        {
            if (dto == null) return null;

            var entity = new Cast();

            entity.CastID = dto.CastID;
            entity.Bio = dto.Bio;
            entity.Bust = dto.Bust;
            entity.Height = dto.Height;
            entity.Hips = dto.Hips;
            entity.Weight = dto.Weight;
            entity.Waist = dto.Waist;
            entity.Sex = dto.Sex;
            entity.BloodType = dto.BloodType;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.CastImage = dto.CastImage;
            entity.ThumbnailImage = dto.ThumbnailImage;
            entity.CastSlug = dto.CastSlug;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Cast"/> to an instance of <see cref="dtoCast"/>.
        /// </summary>
        /// <param name="entity"><see cref="Cast"/> to convert.</param>
        public static dtoCast ToDTO(this Cast entity)
        {
            if (entity == null) return null;

            var dto = new dtoCast();

            dto.CastID = entity.CastID;
            dto.Bio = entity.Bio;
            dto.Height = entity.Height;
            dto.Weight = entity.Weight;
            dto.Bust = entity.Bust;
            dto.Waist = entity.Waist;
            dto.Hips = entity.Hips;
            dto.Sex = entity.Sex;
            dto.BloodType = entity.BloodType;
            dto.FirstName = entity.FirstName;
            dto.LastName = entity.LastName;
            dto.CastImage = entity.CastImage;
            dto.ThumbnailImage = entity.ThumbnailImage;
            dto.CastSlug = entity.CastSlug;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoCast"/> to an instance of <see cref="Cast"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Cast> ToEntities(this IEnumerable<dtoCast> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Cast"/> to an instance of <see cref="dtoCast"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoCast> ToDTOs(this IEnumerable<Cast> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Cast"/> to an instance of <see cref="dtoCast"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoCast> ToQueryDTOs(this IQueryable<Cast> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
