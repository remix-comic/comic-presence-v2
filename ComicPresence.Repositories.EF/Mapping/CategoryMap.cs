using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Category"/> and <see cref="dtoCategory"/>.
    /// </summary>
    public static partial class CategoryMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoCategory"/> converted from <see cref="Category"/>.</param>
        static partial void OnDTO(this Category entity, dtoCategory dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Category"/> converted from <see cref="dtoCategory"/>.</param>
        static partial void OnEntity(this dtoCategory dto, Category entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoCategory"/> to an instance of <see cref="Category"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoCategory"/> to convert.</param>
        public static Category ToEntity(this dtoCategory dto)
        {
            if (dto == null) return null;

            var entity = new Category();

            entity.Id = dto.Id;
            entity.Name = dto.Name;
            entity.UrlSlug = dto.UrlSlug;
            entity.Description = dto.Description;
            entity.Used = dto.Used;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Category"/> to an instance of <see cref="dtoCategory"/>.
        /// </summary>
        /// <param name="entity"><see cref="Category"/> to convert.</param>
        public static dtoCategory ToDTO(this Category entity)
        {
            if (entity == null) return null;

            var dto = new dtoCategory();

            dto.Id = entity.Id;
            dto.Name = entity.Name;
            dto.UrlSlug = entity.UrlSlug;
            dto.Description = entity.Description;
            dto.Used = entity.Used;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoCategory"/> to an instance of <see cref="Category"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Category> ToEntities(this IEnumerable<dtoCategory> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Category"/> to an instance of <see cref="dtoCategory"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoCategory> ToDTOs(this IEnumerable<Category> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Category"/> to an instance of <see cref="dtoCategory"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoCategory> ToQueryDTOs(this IQueryable<Category> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }

    }
}
