using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Chapter"/> and <see cref="dtoChapter"/>.
    /// </summary>
    public static partial class ChapterMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoChapter"/> converted from <see cref="Chapter"/>.</param>
        static partial void OnDTO(this Chapter entity, dtoChapter dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Chapter"/> converted from <see cref="dtoChapter"/>.</param>
        static partial void OnEntity(this dtoChapter dto, Chapter entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoChapter"/> to an instance of <see cref="Chapter"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoChapter"/> to convert.</param>
        public static Chapter ToEntity(this dtoChapter dto)
        {
            if (dto == null) return null;

            var entity = new Chapter();

            entity.BookId = dto.BookId;
            entity.ChapterID = dto.ChapterID;
            entity.ChapterNumber = dto.ChapterNumber;
            entity.ChapterTitle = dto.ChapterTitle;
            entity.ChapterSlug = dto.ChapterSlug;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Chapter"/> to an instance of <see cref="dtoChapter"/>.
        /// </summary>
        /// <param name="entity"><see cref="Chapter"/> to convert.</param>
        public static dtoChapter ToDTO(this Chapter entity)
        {
            if (entity == null) return null;

            var dto = new dtoChapter();

            dto.BookId = entity.BookId;
            dto.ChapterID = entity.ChapterID;
            dto.ChapterNumber = entity.ChapterNumber;
            dto.ChapterTitle = entity.ChapterTitle;
            dto.ChapterSlug = entity.ChapterSlug;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoChapter"/> to an instance of <see cref="Chapter"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Chapter> ToEntities(this IEnumerable<dtoChapter> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Chapter"/> to an instance of <see cref="dtoChapter"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoChapter> ToDTOs(this IEnumerable<Chapter> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Chapter"/> to an instance of <see cref="dtoChapter"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoChapter> ToQueryDTOs(this IQueryable<Chapter> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
