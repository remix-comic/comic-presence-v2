using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="ComicCast"/> and <see cref="dtoComicCast"/>.
    /// </summary>
    public static partial class ComicCastMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoComicCast"/> converted from <see cref="ComicCast"/>.</param>
        static partial void OnDTO(this ComicCast entity, dtoComicCast dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="ComicCast"/> converted from <see cref="dtoComicCast"/>.</param>
        static partial void OnEntity(this dtoComicCast dto, ComicCast entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoComicCast"/> to an instance of <see cref="ComicCast"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoComicCast"/> to convert.</param>
        public static ComicCast ToEntity(this dtoComicCast dto)
        {
            if (dto == null) return null;

            var entity = new ComicCast();

            entity.RowID = dto.RowID;
            entity.CastID = dto.CastID;
            entity.ComicID = dto.ComicID;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="ComicCast"/> to an instance of <see cref="dtoComicCast"/>.
        /// </summary>
        /// <param name="entity"><see cref="ComicCast"/> to convert.</param>
        public static dtoComicCast ToDTO(this ComicCast entity)
        {
            if (entity == null) return null;

            var dto = new dtoComicCast();

            dto.RowID = entity.RowID;
            dto.CastID = entity.CastID;
            dto.ComicID = entity.ComicID;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoComicCast"/> to an instance of <see cref="ComicCast"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<ComicCast> ToEntities(this IEnumerable<dtoComicCast> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="ComicCast"/> to an instance of <see cref="dtoComicCast"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoComicCast> ToDTOs(this IEnumerable<ComicCast> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="ComicCast"/> to an instance of <see cref="dtoComicCast"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoComicCast> ToQueryDTOs(this IQueryable<ComicCast> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
