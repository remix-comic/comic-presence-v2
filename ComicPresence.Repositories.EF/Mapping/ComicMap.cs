using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Comic"/> and <see cref="dtoComic"/>.
    /// </summary>
    public static partial class ComicMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoComic"/> converted from <see cref="Comic"/>.</param>
        static partial void OnDTO(this Comic entity, dtoComic dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Comic"/> converted from <see cref="dtoComic"/>.</param>
        static partial void OnEntity(this dtoComic dto, Comic entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoComic"/> to an instance of <see cref="Comic"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoComic"/> to convert.</param>
        public static Comic ToEntity(this dtoComic dto)
        {
            if (dto == null) return null;

            var entity = new Comic();

            entity.ComicID = dto.ComicID;
            entity.AuthorID = dto.AuthorID;
            entity.IllustratorID = dto.IllustratorID;
            entity.WriterID = dto.WriterID;
            entity.ChapterID = dto.ChapterID;
            entity.BookID = dto.BookID;
            entity.Title = dto.Title;
            entity.LongDescription = dto.LongDescription;
            entity.ShortDescription = dto.ShortDescription;
            entity.ComicImage = dto.ComicImage;
            entity.ThumbImage = dto.ThumbImage;
            entity.BannerImage = dto.BannerImage;
            entity.IsArchived = dto.IsArchived;
            entity.IsLastComic = dto.IsLastComic;
            entity.PublishTime = dto.PublishTime;            
            entity.OnHomePage = dto.OnHomePage;
            entity.ComicDate = dto.ComicDate;
            entity.ComicSlug = dto.ComicSlug;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Comic"/> to an instance of <see cref="dtoComic"/>.
        /// </summary>
        /// <param name="entity"><see cref="Comic"/> to convert.</param>
        public static dtoComic ToDTO(this Comic entity)
        {
            if (entity == null) return null;

            var dto = new dtoComic();

            dto.ComicID = entity.ComicID;
            dto.AuthorID = entity.AuthorID;
            dto.IllustratorID = entity.IllustratorID;
            dto.WriterID = entity.WriterID;
            dto.ChapterID = entity.ChapterID;
            dto.BookID = entity.BookID;
            dto.Title = entity.Title;
            dto.LongDescription = entity.LongDescription;
            dto.ShortDescription = entity.ShortDescription;
            dto.ComicImage = entity.ComicImage;
            dto.ThumbImage = entity.ThumbImage;
            dto.BannerImage = entity.BannerImage;
            dto.IsArchived = entity.IsArchived;
            dto.IsLastComic = entity.IsLastComic;
            dto.OnHomePage = entity.OnHomePage;
            dto.ComicDate = entity.ComicDate;
            dto.PublishTime = entity.PublishTime;              
            dto.ComicSlug = entity.ComicSlug;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoComic"/> to an instance of <see cref="Comic"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Comic> ToEntities(this IEnumerable<dtoComic> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Comic"/> to an instance of <see cref="dtoComic"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoComic> ToDTOs(this IEnumerable<Comic> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Comic"/> to an instance of <see cref="dtoComic"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoComic> ToQueryDTOs(this IQueryable<Comic> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
