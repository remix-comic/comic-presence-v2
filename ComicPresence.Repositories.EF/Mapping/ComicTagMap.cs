using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="ComicTag"/> and <see cref="dtoComicTag"/>.
    /// </summary>
    public static partial class ComicTagMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoComicTag"/> converted from <see cref="ComicTag"/>.</param>
        static partial void OnDTO(this ComicTag entity, dtoComicTag dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="ComicTag"/> converted from <see cref="dtoComicTag"/>.</param>
        static partial void OnEntity(this dtoComicTag dto, ComicTag entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoComicTag"/> to an instance of <see cref="ComicTag"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoComicTag"/> to convert.</param>
        public static ComicTag ToEntity(this dtoComicTag dto)
        {
            if (dto == null) return null;

            var entity = new ComicTag();

            entity.RowId = dto.RowId;
            entity.ComicId = dto.ComicId;
            entity.TagId = dto.TagId;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="ComicTag"/> to an instance of <see cref="dtoComicTag"/>.
        /// </summary>
        /// <param name="entity"><see cref="ComicTag"/> to convert.</param>
        public static dtoComicTag ToDTO(this ComicTag entity)
        {
            if (entity == null) return null;

            var dto = new dtoComicTag();

            dto.RowId = entity.RowId;
            dto.ComicId = entity.ComicId;
            dto.TagId = entity.TagId;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoComicTag"/> to an instance of <see cref="ComicTag"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<ComicTag> ToEntities(this IEnumerable<dtoComicTag> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="ComicTag"/> to an instance of <see cref="dtoComicTag"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoComicTag> ToDTOs(this IEnumerable<ComicTag> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="ComicTag"/> to an instance of <see cref="dtoComicTag"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoComicTag> ToQueryDTOs(this IQueryable<ComicTag> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
