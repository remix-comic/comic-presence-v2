using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Link"/> and <see cref="dtoLink"/>.
    /// </summary>
    public static partial class LinkMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoLink"/> converted from <see cref="Link"/>.</param>
        static partial void OnDTO(this Link entity, dtoLink dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Link"/> converted from <see cref="dtoLink"/>.</param>
        static partial void OnEntity(this dtoLink dto, Link entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoLink"/> to an instance of <see cref="Link"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoLink"/> to convert.</param>
        public static Link ToEntity(this dtoLink dto)
        {
            if (dto == null) return null;

            var entity = new Link();

            entity.LinkId = dto.LinkId;
            entity.AlternateName = dto.AlternateName;
            entity.ExternalUrl = dto.ExternalUrl;
            entity.ImagePath = dto.ImagePath;
            entity.IsActive = dto.IsActive;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Link"/> to an instance of <see cref="dtoLink"/>.
        /// </summary>
        /// <param name="entity"><see cref="Link"/> to convert.</param>
        public static dtoLink ToDTO(this Link entity)
        {
            if (entity == null) return null;

            var dto = new dtoLink();

            dto.LinkId = entity.LinkId;
            dto.AlternateName = entity.AlternateName;
            dto.ExternalUrl = entity.ExternalUrl;
            dto.ImagePath = entity.ImagePath;
            dto.IsActive = entity.IsActive;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoLink"/> to an instance of <see cref="Link"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Link> ToEntities(this IEnumerable<dtoLink> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Link"/> to an instance of <see cref="dtoLink"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoLink> ToDTOs(this IEnumerable<Link> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Link"/> to an instance of <see cref="dtoLink"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoLink> ToQueryDTOs(this IQueryable<Link> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
