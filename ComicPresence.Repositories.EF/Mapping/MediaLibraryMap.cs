﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{
    /// <summary>
    /// Map for <see cref="MediaLibrary"/> and <see cref="dtoMediaLibrary"/>.
    /// </summary>
    public static partial class MediaLibraryMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoMediaLibrary"/> converted from <see cref="MediaLibrary"/>.</param>
        static partial void OnDTO(this MediaLibrary entity, dtoMediaLibrary dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="MediaLibrary"/> converted from <see cref="dtoMediaLibrary"/>.</param>
        static partial void OnEntity(this dtoMediaLibrary dto, MediaLibrary entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoMediaLibrary"/> to an instance of <see cref="MediaLibrary"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoMediaLibrary"/> to convert.</param>
        public static MediaLibrary ToEntity(this dtoMediaLibrary dto)
        {
            if (dto == null) return null;

            var entity = new MediaLibrary();

            entity.Author = dto.Author;
            entity.ImagePath = dto.ImagePath;
            entity.MediaId = dto.MediaId;
            entity.MediaType = dto.MediaType;
            entity.Name = dto.Name;
            entity.Slug = dto.Slug;
            entity.ThumbPath = dto.ThumbPath;
            entity.Extention = dto.Extention;
            entity.Dimensions = dto.Dimensions;
            entity.Uploaded = dto.Uploaded;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="MediaLibrary"/> to an instance of <see cref="dtoMediaLibrary"/>.
        /// </summary>
        /// <param name="entity"><see cref="MediaLibrary"/> to convert.</param>
        public static dtoMediaLibrary ToDTO(this MediaLibrary entity)
        {
            if (entity == null) return null;

            var dto = new dtoMediaLibrary();

            dto.Author = entity.Author;
            dto.ImagePath = entity.ImagePath;
            dto.MediaId = entity.MediaId;
            dto.MediaType = entity.MediaType;
            dto.Name = entity.Name;
            dto.Slug = entity.Slug;
            dto.ThumbPath = entity.ThumbPath;
            dto.Extention = entity.Extention;
            dto.Dimensions = entity.Dimensions;
            dto.Uploaded = entity.Uploaded;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoMediaLibrary"/> to an instance of <see cref="MediaLibrary"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<MediaLibrary> ToEntities(this IEnumerable<dtoMediaLibrary> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="MediaLibrary"/> to an instance of <see cref="dtoMediaLibrary"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoMediaLibrary> ToDTOs(this IEnumerable<MediaLibrary> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="MediaLibrary"/> to an instance of <see cref="dtoMediaLibrary"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoMediaLibrary> ToQueryDTOs(this IQueryable<MediaLibrary> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
