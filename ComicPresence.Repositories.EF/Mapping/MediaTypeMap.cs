﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{
    /// <summary>
    /// Map for <see cref="MediaType"/> and <see cref="dtoMediaType"/>.
    /// </summary>
    public static partial class MediaTypeMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoMediaType"/> converted from <see cref="MediaType"/>.</param>
        static partial void OnDTO(this MediaType entity, dtoMediaType dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="MediaType"/> converted from <see cref="dtoMediaType"/>.</param>
        static partial void OnEntity(this dtoMediaType dto, MediaType entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoMediaType"/> to an instance of <see cref="MediaType"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoMediaType"/> to convert.</param>
        public static MediaType ToEntity(this dtoMediaType dto)
        {
            if (dto == null) return null;

            var entity = new MediaType();

            entity.Description = dto.Description;
            entity.Slug = dto.Slug;
            entity.TypeId = dto.TypeId;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="MediaType"/> to an instance of <see cref="dtoMediaType"/>.
        /// </summary>
        /// <param name="entity"><see cref="MediaType"/> to convert.</param>
        public static dtoMediaType ToDTO(this MediaType entity)
        {
            if (entity == null) return null;

            var dto = new dtoMediaType();

            dto.Description = entity.Description;
            dto.Slug = entity.Slug;
            dto.TypeId = entity.TypeId;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoMediaType"/> to an instance of <see cref="MediaType"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<MediaType> ToEntities(this IEnumerable<dtoMediaType> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="MediaType"/> to an instance of <see cref="dtoMediaType"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoMediaType> ToDTOs(this IEnumerable<MediaType> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="MediaType"/> to an instance of <see cref="dtoMediaType"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoMediaType> ToQueryDTOs(this IQueryable<MediaType> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
