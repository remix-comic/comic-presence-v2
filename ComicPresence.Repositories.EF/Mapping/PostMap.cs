using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Post"/> and <see cref="dtoPost"/>.
    /// </summary>
    public static partial class PostMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoPost"/> converted from <see cref="Post"/>.</param>
        static partial void OnDTO(this Post entity, dtoPost dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Post"/> converted from <see cref="dtoPost"/>.</param>
        static partial void OnEntity(this dtoPost dto, Post entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoPost"/> to an instance of <see cref="Post"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoPost"/> to convert.</param>
        public static Post ToEntity(this dtoPost dto)
        {
            if (dto == null) return null;

            var entity = new Post();

            entity.Id = dto.Id;
            entity.BlogId = dto.BlogId;
            entity.Title = dto.Title;
            entity.ShortDescription = dto.ShortDescription;
            entity.LongDescription = dto.LongDescription;
            entity.Meta = dto.Meta;
            entity.UrlSlug = dto.UrlSlug;
            entity.Published = dto.Published;
            entity.PostedOn = dto.PostedOn;
            entity.Modified = dto.Modified;
            entity.CategoryId = dto.CategoryId;
            entity.Status = dto.Status;
            entity.Visibility = dto.Visibility;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Post"/> to an instance of <see cref="dtoPost"/>.
        /// </summary>
        /// <param name="entity"><see cref="Post"/> to convert.</param>
        public static dtoPost ToDTO(this Post entity)
        {
            if (entity == null) return null;

            var dto = new dtoPost();

            dto.Id = entity.Id;
            dto.BlogId = entity.BlogId;
            dto.Title = entity.Title;
            dto.ShortDescription = entity.ShortDescription;
            dto.LongDescription = entity.LongDescription;
            dto.Meta = entity.Meta;
            dto.UrlSlug = entity.UrlSlug;
            dto.Published = entity.Published;
            dto.PostedOn = entity.PostedOn;
            dto.Modified = entity.Modified;
            dto.CategoryId = entity.CategoryId;
            dto.Status = entity.Status;
            dto.Visibility = entity.Visibility;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoPost"/> to an instance of <see cref="Post"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Post> ToEntities(this IEnumerable<dtoPost> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Post"/> to an instance of <see cref="dtoPost"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoPost> ToDTOs(this IEnumerable<Post> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Post"/> to an instance of <see cref="dtoPost"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoPost> ToQueryDTOs(this IQueryable<Post> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }

    }
}
