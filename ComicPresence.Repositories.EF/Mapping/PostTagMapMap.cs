using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="PostTagMap"/> and <see cref="dtoPostTagMap"/>.
    /// </summary>
    public static partial class PostTagMapMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoPostTagMap"/> converted from <see cref="PostTagMap"/>.</param>
        static partial void OnDTO(this PostTagMap entity, dtoPostTagMap dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="PostTagMap"/> converted from <see cref="dtoPostTagMap"/>.</param>
        static partial void OnEntity(this dtoPostTagMap dto, PostTagMap entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoPostTagMap"/> to an instance of <see cref="PostTagMap"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoPostTagMap"/> to convert.</param>
        public static PostTagMap ToEntity(this dtoPostTagMap dto)
        {
            if (dto == null) return null;

            var entity = new PostTagMap();

            entity.RowId = dto.RowId;
            entity.PostId = dto.PostId;
            entity.TagId = dto.TagId;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="PostTagMap"/> to an instance of <see cref="dtoPostTagMap"/>.
        /// </summary>
        /// <param name="entity"><see cref="PostTagMap"/> to convert.</param>
        public static dtoPostTagMap ToDTO(this PostTagMap entity)
        {
            if (entity == null) return null;

            var dto = new dtoPostTagMap();

            dto.RowId = entity.RowId;
            dto.PostId = entity.PostId;
            dto.TagId = entity.TagId;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoPostTagMap"/> to an instance of <see cref="PostTagMap"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<PostTagMap> ToEntities(this IEnumerable<dtoPostTagMap> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="PostTagMap"/> to an instance of <see cref="dtoPostTagMap"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoPostTagMap> ToDTOs(this IEnumerable<PostTagMap> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="PostTagMap"/> to an instance of <see cref="dtoPostTagMap"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoPostTagMap> ToQueryDTOs(this IQueryable<PostTagMap> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }

    }
}
