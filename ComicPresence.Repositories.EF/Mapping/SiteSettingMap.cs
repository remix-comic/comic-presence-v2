using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="SiteSetting"/> and <see cref="dtoSiteSetting"/>.
    /// </summary>
    public static partial class SiteSettingMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoSiteSetting"/> converted from <see cref="SiteSetting"/>.</param>
        static partial void OnDTO(this SiteSetting entity, dtoSiteSetting dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="SiteSetting"/> converted from <see cref="dtoSiteSetting"/>.</param>
        static partial void OnEntity(this dtoSiteSetting dto, SiteSetting entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoSiteSetting"/> to an instance of <see cref="SiteSetting"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoSiteSetting"/> to convert.</param>
        public static SiteSetting ToEntity(this dtoSiteSetting dto)
        {
            if (dto == null) return null;

            var entity = new SiteSetting();

            entity.ConfigurationKey = dto.ConfigurationKey;
            entity.Description = dto.Description;
            entity.Value = dto.Value;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="SiteSetting"/> to an instance of <see cref="dtoSiteSetting"/>.
        /// </summary>
        /// <param name="entity"><see cref="SiteSetting"/> to convert.</param>
        public static dtoSiteSetting ToDTO(this SiteSetting entity)
        {
            if (entity == null) return null;

            var dto = new dtoSiteSetting();

            dto.ConfigurationKey = entity.ConfigurationKey;
            dto.Description = entity.Description;
            dto.Value = entity.Value;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoSiteSetting"/> to an instance of <see cref="SiteSetting"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<SiteSetting> ToEntities(this IEnumerable<dtoSiteSetting> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="SiteSetting"/> to an instance of <see cref="dtoSiteSetting"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoSiteSetting> ToDTOs(this IEnumerable<SiteSetting> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="SiteSetting"/> to an instance of <see cref="dtoSiteSetting"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoSiteSetting> ToQueryDTOs(this IQueryable<SiteSetting> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }

    }
}
