using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Tag"/> and <see cref="dtoTag"/>.
    /// </summary>
    public static partial class TagMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoTag"/> converted from <see cref="Tag"/>.</param>
        static partial void OnDTO(this Tag entity, dtoTag dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Tag"/> converted from <see cref="dtoTag"/>.</param>
        static partial void OnEntity(this dtoTag dto, Tag entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoTag"/> to an instance of <see cref="Tag"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoTag"/> to convert.</param>
        public static Tag ToEntity(this dtoTag dto)
        {
            if (dto == null) return null;

            var entity = new Tag();

            entity.TagId = dto.TagId;
            entity.TagName = dto.TagName;
            entity.TagSlug = dto.TagSlug;
            entity.TagCount = dto.TagCount;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Tag"/> to an instance of <see cref="dtoTag"/>.
        /// </summary>
        /// <param name="entity"><see cref="Tag"/> to convert.</param>
        public static dtoTag ToDTO(this Tag entity)
        {
            if (entity == null) return null;

            var dto = new dtoTag();

            dto.TagId = entity.TagId;
            dto.TagName = entity.TagName;
            dto.TagSlug = entity.TagSlug;
            dto.TagCount = entity.TagCount;         

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoTag"/> to an instance of <see cref="Tag"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Tag> ToEntities(this IEnumerable<dtoTag> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Tag"/> to an instance of <see cref="dtoTag"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoTag> ToDTOs(this IEnumerable<Tag> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Tag"/> to an instance of <see cref="dtoTag"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoTag> ToQueryDTOs(this IQueryable<Tag> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }

    }
}
