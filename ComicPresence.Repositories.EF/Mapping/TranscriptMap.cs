using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="Transcript"/> and <see cref="dtoTranscript"/>.
    /// </summary>
    public static partial class TranscriptMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoTranscript"/> converted from <see cref="Transcript"/>.</param>
        static partial void OnDTO(this Transcript entity, dtoTranscript dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Transcript"/> converted from <see cref="dtoTranscript"/>.</param>
        static partial void OnEntity(this dtoTranscript dto, Transcript entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoTranscript"/> to an instance of <see cref="Transcript"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoTranscript"/> to convert.</param>
        public static Transcript ToEntity(this dtoTranscript dto)
        {
            if (dto == null) return null;

            var entity = new Transcript();

            entity.TranscriptId = dto.TranscriptId;
            entity.ScriptText = dto.ScriptText;
            entity.ParentComic = dto.ParentComic;
            entity.AuthorId = dto.AuthorId;
            entity.Slug = dto.Slug;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Transcript"/> to an instance of <see cref="dtoTranscript"/>.
        /// </summary>
        /// <param name="entity"><see cref="Transcript"/> to convert.</param>
        public static dtoTranscript ToDTO(this Transcript entity)
        {
            if (entity == null) return null;

            var dto = new dtoTranscript();

            dto.TranscriptId = entity.TranscriptId;
            dto.ScriptText = entity.ScriptText;
            dto.ParentComic = entity.ParentComic;
            dto.AuthorId = entity.AuthorId;
            dto.Slug = entity.Slug;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoTranscript"/> to an instance of <see cref="Transcript"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Transcript> ToEntities(this IEnumerable<dtoTranscript> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Transcript"/> to an instance of <see cref="dtoTranscript"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoTranscript> ToDTOs(this IEnumerable<Transcript> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Transcript"/> to an instance of <see cref="dtoTranscript"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoTranscript> ToQueryDTOs(this IQueryable<Transcript> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
