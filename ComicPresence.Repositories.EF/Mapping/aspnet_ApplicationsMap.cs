using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="aspnet_Applications"/> and <see cref="dtoaspnet_Applications"/>.
    /// </summary>
    public static partial class aspnet_ApplicationsMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Applications"/> converted from <see cref="aspnet_Applications"/>.</param>
        static partial void OnDTO(this aspnet_Applications entity, dtoaspnet_Applications dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Applications"/> converted from <see cref="dtoaspnet_Applications"/>.</param>
        static partial void OnEntity(this dtoaspnet_Applications dto, aspnet_Applications entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_Applications"/> to an instance of <see cref="aspnet_Applications"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Applications"/> to convert.</param>
        public static aspnet_Applications ToEntity(this dtoaspnet_Applications dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_Applications();

            entity.ApplicationName = dto.ApplicationName;
            entity.LoweredApplicationName = dto.LoweredApplicationName;
            entity.ApplicationId = dto.ApplicationId;
            entity.Description = dto.Description;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_Applications"/> to an instance of <see cref="dtoaspnet_Applications"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Applications"/> to convert.</param>
        public static dtoaspnet_Applications ToDTO(this aspnet_Applications entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_Applications();

            dto.ApplicationName = entity.ApplicationName;
            dto.LoweredApplicationName = entity.LoweredApplicationName;
            dto.ApplicationId = entity.ApplicationId;
            dto.Description = entity.Description;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_Applications"/> to an instance of <see cref="aspnet_Applications"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_Applications> ToEntities(this IEnumerable<dtoaspnet_Applications> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Applications"/> to an instance of <see cref="dtoaspnet_Applications"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_Applications> ToDTOs(this IEnumerable<aspnet_Applications> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Applications"/> to an instance of <see cref="dtoaspnet_Applications"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_Applications> ToQueryDTOs(this IQueryable<aspnet_Applications> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
