using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="aspnet_Membership"/> and <see cref="dtoaspnet_Membership"/>.
    /// </summary>
    public static partial class aspnet_MembershipMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Membership"/> converted from <see cref="aspnet_Membership"/>.</param>
        static partial void OnDTO(this aspnet_Membership entity, dtoaspnet_Membership dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Membership"/> converted from <see cref="dtoaspnet_Membership"/>.</param>
        static partial void OnEntity(this dtoaspnet_Membership dto, aspnet_Membership entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_Membership"/> to an instance of <see cref="aspnet_Membership"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Membership"/> to convert.</param>
        public static aspnet_Membership ToEntity(this dtoaspnet_Membership dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_Membership();

            entity.ApplicationId = dto.ApplicationId;
            entity.UserId = dto.UserId;
            entity.AuthorId = dto.AuthorId;
            entity.Password = dto.Password;
            entity.PasswordFormat = dto.PasswordFormat;
            entity.PasswordSalt = dto.PasswordSalt;
            entity.MobilePIN = dto.MobilePIN;
            entity.Email = dto.Email;
            entity.LoweredEmail = dto.LoweredEmail;
            entity.PasswordQuestion = dto.PasswordQuestion;
            entity.PasswordAnswer = dto.PasswordAnswer;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.IsApproved = dto.IsApproved;
            entity.IsLockedOut = dto.IsLockedOut;
            entity.Comment = dto.Comment;
            entity.Bio = dto.Bio;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_Membership"/> to an instance of <see cref="dtoaspnet_Membership"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Membership"/> to convert.</param>
        public static dtoaspnet_Membership ToDTO(this aspnet_Membership entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_Membership();

            dto.ApplicationId = entity.ApplicationId;
            dto.UserId = entity.UserId;
            dto.AuthorId = entity.AuthorId;
            dto.Password = entity.Password;
            dto.PasswordFormat = entity.PasswordFormat;
            dto.PasswordSalt = entity.PasswordSalt;
            dto.MobilePIN = entity.MobilePIN;
            dto.Email = entity.Email;
            dto.LoweredEmail = entity.LoweredEmail;
            dto.PasswordQuestion = entity.PasswordQuestion;
            dto.PasswordAnswer = entity.PasswordAnswer;
            dto.FirstName = entity.FirstName;
            dto.LastName = entity.LastName;
            dto.IsApproved = entity.IsApproved;
            dto.IsLockedOut = entity.IsLockedOut;
            dto.Comment = entity.Comment;
            dto.Bio = entity.Bio;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_Membership"/> to an instance of <see cref="aspnet_Membership"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_Membership> ToEntities(this IEnumerable<dtoaspnet_Membership> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Membership"/> to an instance of <see cref="dtoaspnet_Membership"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_Membership> ToDTOs(this IEnumerable<aspnet_Membership> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Membership"/> to an instance of <see cref="dtoaspnet_Membership"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_Membership> ToQueryDTOs(this IQueryable<aspnet_Membership> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
