using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="aspnet_Profile"/> and <see cref="dtoaspnet_Profile"/>.
    /// </summary>
    public static partial class aspnet_ProfileMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Profile"/> converted from <see cref="aspnet_Profile"/>.</param>
        static partial void OnDTO(this aspnet_Profile entity, dtoaspnet_Profile dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Profile"/> converted from <see cref="dtoaspnet_Profile"/>.</param>
        static partial void OnEntity(this dtoaspnet_Profile dto, aspnet_Profile entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_Profile"/> to an instance of <see cref="aspnet_Profile"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Profile"/> to convert.</param>
        public static aspnet_Profile ToEntity(this dtoaspnet_Profile dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_Profile();

            entity.UserId = dto.UserId;
            entity.PropertyNames = dto.PropertyNames;
            entity.PropertyValuesString = dto.PropertyValuesstring;
            entity.PropertyValuesBinary = dto.PropertyValuesBinary;
            entity.LastUpdatedDate = dto.LastUpdatedDate;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_Profile"/> to an instance of <see cref="dtoaspnet_Profile"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Profile"/> to convert.</param>
        public static dtoaspnet_Profile ToDTO(this aspnet_Profile entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_Profile();

            dto.UserId = entity.UserId;
            dto.PropertyNames = entity.PropertyNames;
            dto.PropertyValuesstring = entity.PropertyValuesString;
            dto.PropertyValuesBinary = entity.PropertyValuesBinary;
            dto.LastUpdatedDate = entity.LastUpdatedDate;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_Profile"/> to an instance of <see cref="aspnet_Profile"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_Profile> ToEntities(this IEnumerable<dtoaspnet_Profile> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Profile"/> to an instance of <see cref="dtoaspnet_Profile"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_Profile> ToDTOs(this IEnumerable<aspnet_Profile> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Profile"/> to an instance of <see cref="dtoaspnet_Profile"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_Profile> ToQueryDTOs(this IQueryable<aspnet_Profile> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
