using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="aspnet_Roles"/> and <see cref="dtoaspnet_Roles"/>.
    /// </summary>
    public static partial class aspnet_RolesMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Roles"/> converted from <see cref="aspnet_Roles"/>.</param>
        static partial void OnDTO(this aspnet_Roles entity, dtoaspnet_Roles dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Roles"/> converted from <see cref="dtoaspnet_Roles"/>.</param>
        static partial void OnEntity(this dtoaspnet_Roles dto, aspnet_Roles entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_Roles"/> to an instance of <see cref="aspnet_Roles"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Roles"/> to convert.</param>
        public static aspnet_Roles ToEntity(this dtoaspnet_Roles dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_Roles();

            entity.ApplicationId = dto.ApplicationId;
            entity.RoleId = dto.RoleId;
            entity.RoleName = dto.RoleName;
            entity.LoweredRoleName = dto.LoweredRoleName;
            entity.Description = dto.Description;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_Roles"/> to an instance of <see cref="dtoaspnet_Roles"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Roles"/> to convert.</param>
        public static dtoaspnet_Roles ToDTO(this aspnet_Roles entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_Roles();

            dto.ApplicationId = entity.ApplicationId;
            dto.RoleId = entity.RoleId;
            dto.RoleName = entity.RoleName;
            dto.LoweredRoleName = entity.LoweredRoleName;
            dto.Description = entity.Description;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_Roles"/> to an instance of <see cref="aspnet_Roles"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_Roles> ToEntities(this IEnumerable<dtoaspnet_Roles> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Roles"/> to an instance of <see cref="dtoaspnet_Roles"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_Roles> ToDTOs(this IEnumerable<aspnet_Roles> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Roles"/> to an instance of <see cref="dtoaspnet_Roles"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_Roles> ToQueryDTOs(this IQueryable<aspnet_Roles> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
