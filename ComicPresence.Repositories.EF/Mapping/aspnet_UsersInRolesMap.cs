using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="aspnet_UsersInRoles"/> and <see cref="dtoaspnet_UsersInRoles"/>.
    /// </summary>
    public static partial class aspnet_UsersInRolesMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_UsersInRoles"/> converted from <see cref="aspnet_UsersInRoles"/>.</param>
        static partial void OnDTO(this aspnet_UsersInRoles entity, dtoaspnet_UsersInRoles dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_UsersInRoles"/> converted from <see cref="dtoaspnet_UsersInRoles"/>.</param>
        static partial void OnEntity(this dtoaspnet_UsersInRoles dto, aspnet_UsersInRoles entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_UsersInRoles"/> to an instance of <see cref="aspnet_UsersInRoles"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_UsersInRoles"/> to convert.</param>
        public static aspnet_UsersInRoles ToEntity(this dtoaspnet_UsersInRoles dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_UsersInRoles();

            entity.RowId = dto.RowId;
            entity.UserId = dto.UserId;
            entity.RoleId = dto.RoleId;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_UsersInRoles"/> to an instance of <see cref="dtoaspnet_UsersInRoles"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_UsersInRoles"/> to convert.</param>
        public static dtoaspnet_UsersInRoles ToDTO(this aspnet_UsersInRoles entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_UsersInRoles();

            dto.RowId = entity.RowId;
            dto.UserId = entity.UserId;
            dto.RoleId = entity.RoleId;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_UsersInRoles"/> to an instance of <see cref="aspnet_UsersInRoles"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_UsersInRoles> ToEntities(this IEnumerable<dtoaspnet_UsersInRoles> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_UsersInRoles"/> to an instance of <see cref="dtoaspnet_UsersInRoles"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_UsersInRoles> ToDTOs(this IEnumerable<aspnet_UsersInRoles> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_UsersInRoles"/> to an instance of <see cref="dtoaspnet_UsersInRoles"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_UsersInRoles> ToQueryDTOs(this IQueryable<aspnet_UsersInRoles> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
