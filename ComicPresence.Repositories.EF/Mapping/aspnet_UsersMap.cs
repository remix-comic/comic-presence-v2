using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Model;

namespace ComicPresence.Repositories.EF.Mapping
{

    /// <summary>
    /// Map for <see cref="aspnet_Users"/> and <see cref="dtoaspnet_Users"/>.
    /// </summary>
    public static partial class aspnet_UsersMap
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Users"/> converted from <see cref="aspnet_Users"/>.</param>
        static partial void OnDTO(this aspnet_Users entity, dtoaspnet_Users dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Users"/> converted from <see cref="dtoaspnet_Users"/>.</param>
        static partial void OnEntity(this dtoaspnet_Users dto, aspnet_Users entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_Users"/> to an instance of <see cref="aspnet_Users"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Users"/> to convert.</param>
        public static aspnet_Users ToEntity(this dtoaspnet_Users dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_Users();

            entity.ApplicationId = dto.ApplicationId;
            entity.UserId = dto.UserId;
            entity.UserName = dto.UserName;
            entity.LoweredUserName = dto.LoweredUserName;
            entity.MobileAlias = dto.MobileAlias;
            entity.IsAnonymous = dto.IsAnonymous;
            entity.LastActivityDate = dto.LastActivityDate;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_Users"/> to an instance of <see cref="dtoaspnet_Users"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Users"/> to convert.</param>
        public static dtoaspnet_Users ToDTO(this aspnet_Users entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_Users();

            dto.ApplicationId = entity.ApplicationId;
            dto.UserId = entity.UserId;
            dto.UserName = entity.UserName;
            dto.LoweredUserName = entity.LoweredUserName;
            dto.MobileAlias = entity.MobileAlias;
            dto.IsAnonymous = entity.IsAnonymous;
            dto.LastActivityDate = entity.LastActivityDate;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_Users"/> to an instance of <see cref="aspnet_Users"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_Users> ToEntities(this IEnumerable<dtoaspnet_Users> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Users"/> to an instance of <see cref="dtoaspnet_Users"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_Users> ToDTOs(this IEnumerable<aspnet_Users> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Users"/> to an instance of <see cref="dtoaspnet_Users"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_Users> ToQueryDTOs(this IQueryable<aspnet_Users> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
