﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Common;
using ComicPresence.Model;
using ComicPresence.Model.Repositories;
using ComicPresence.Repositories.EF.Mapping;

/// <summary>
/// A concrete repository to work with people in the system.
/// </summary>
namespace ComicPresence.Repositories.EF.Repositories
{
    public class ComicRepository : Repository<dtoComic>, IComicRepository
    {
        public IEnumerable<dtoComic> GetAllComics()
        {
            using( var db = DataContextFactory.GetDataContext())
            {
                try
                {
                    var comics = db.Set<Comic>().ToDTOs();

                    foreach (var comic in comics)
                    {
                        var illustrator = DataContextFactory.GetDataContext().aspnet_Membership.Where(i => i.AuthorId == comic.IllustratorID).FirstOrDefault();
                        var writer = DataContextFactory.GetDataContext().aspnet_Membership.Where(i => i.AuthorId == comic.WriterID).FirstOrDefault();
                        comic.Chapter = DataContextFactory.GetDataContext().Chapters.Where(c => c.ChapterID == comic.ChapterID).FirstOrDefault().ToDTO();
                        comic.IllustratorName = illustrator.FirstName + " " + illustrator.LastName;

                        if (writer != null)
                            comic.WriterName = writer.FirstName + " " + writer.LastName;
                        else
                            comic.WriterName = "N/A";
                    }

                    return comics;
                }
                catch (Exception ex)
                {
                    this.Error(ex.Message, ex);
                    return null;
                }
            }            
        }

        public dtoComic GetComicBySlug(string slug)
        {
            using (var db = DataContextFactory.GetDataContext())
            {
                try
                {
                    return db.Comics.Where(c => c.ComicSlug == slug).FirstOrDefault().ToDTO();
                }
                catch (Exception ex)
                {
                    this.Error(ex.Message, ex);
                    return null;
                }
            }            
        }

        public dtoComic GetCurrentComic()
        {
            using (var db = DataContextFactory.GetDataContext())
            {
                try
                {
                    return db.Comics.Where(c => c.OnHomePage == true).FirstOrDefault().ToDTO();
                }
                catch (Exception ex)
                {
                    this.Error(ex.Message, ex);
                    return null;
                }
            }            
        }
    }
}
