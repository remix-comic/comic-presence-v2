﻿using System;
using System.Linq;
using System.Data.Objects;

namespace ComicPresence.Repositories.EF.Repositories.Interfaces
{
    /// <summary>
    /// Context across all repositories
    /// </summary>
    public interface IRepositoryContext
    {
        IObjectSet<T> GetObjectSet<T>() where T : class;
        ObjectContext ObjectContext { get; } 
        void Clear();
        int SaveChanges();
    }
}
