﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicPresence.Common;
using ComicPresence.Model;
using ComicPresence.Model.Repositories;
using ComicPresence.Repositories.EF.Mapping;

/// <summary>
/// A concrete repository to work with the admin menu in the system.
/// </summary>
namespace ComicPresence.Repositories.EF.Repositories
{
    public class MenuRepository : Repository<dtoMenu>, IMenuRepository
    {
        public dtoMenu GetAdminMenu()
        {
            using (var db = DataContextFactory.GetDataContext())
            {
                try
                {
                    dtoMenu _menu = new dtoMenu();

                    _menu.Comics = db.Set<Comic>().ToDTOs();

                    return _menu;
                }
                catch (Exception ex)
                {
                    this.Error(ex.Message, ex);
                    return null;
                }
            }
        }
    }
}
