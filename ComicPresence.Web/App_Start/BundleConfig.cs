﻿using System.Web.Optimization;

// ReSharper disable CheckNamespace
namespace ComicPresence.Web
// ReSharper restore CheckNamespace
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterStyleBundles(bundles);
            RegisterJavascriptBundles(bundles);            
            RegisterAdminStyleBundles(bundles);
            RegisterAdminFontBundle(bundles);
            RegisterAdminJQuery(bundles);
            RegisterAdminJavascriptBundles(bundles);
            RegisterAdminVendorBundles(bundles);
        }

        private static void RegisterStyleBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css")
                            .Include("~/Content/assets/bootstrap.min.css")
                            .Include("~/Content/assets/bootstrap-responsive.min.css")
                            .Include("~/Content/assets/site.min.css"));
        }

        private static void RegisterJavascriptBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/js")
                            .Include("~/areas/dashboard/Scripts/jquery-{version}.js")
                            .Include("~/areas/dashboard/Scripts/jquery-ui-{version}.js")
                            .Include("~/areas/dashboard/Scripts/bootstrap.js"));
        }

        private static void RegisterAdminStyleBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new StyleBundle("~/dashboard/css")
                .Include("~/areas/dashboard/content/app/app.css")
                .Include("~/areas/dashboard/content/app/bootstrap.css"));
        }

        private static void RegisterAdminFontBundle(BundleCollection bundles)
        {
            bundles.UseCdn = true;
            var cdnPath = "http://fonts.googleapis.com/css?family=Lato:300,400,700";

            bundles.Add(new StyleBundle("~/dashboard/fonts", cdnPath));
        }

        private static void RegisterAdminJQuery(BundleCollection bundles)
        {
            bundles.UseCdn = true;
            var cdn = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js";

            bundles.Add(new ScriptBundle("~/dashboard/jquery", cdn));

            bundles.Add(new ScriptBundle("~/dashboard/modernizr").Include(
                        "~/areas/dashboard/Scripts/modernizr-*"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/bootstrap").Include(
                      "~/areas/dashboard/Scripts/bootstrap.js"
            ));
        }

        private static void RegisterAdminJavascriptBundles(BundleCollection bundles)
        {
            // Base Scripts (not lazyloaded)
            bundles.Add(new ScriptBundle("~/dashboard/baseScripts").Include(
                //App init
                "~/areas/dashboard/Scripts/app/app.init.js",
                // Modules
                "~/areas/dashboard/Scripts/app/modules/bootstrap-start.js",
                "~/areas/dashboard/Scripts/app/modules/calendar.js",
                "~/areas/dashboard/Scripts/app/modules/classyloader.js",
                "~/areas/dashboard/Scripts/app/modules/clear-storage.js",
                "~/areas/dashboard/Scripts/app/modules/constants.js",
                "~/areas/dashboard/Scripts/app/modules/demo",
                "~/areas/dashboard/Scripts/app/modules/flatdoc.js",
                "~/areas/dashboard/Scripts/app/modules/fullscreen.js",
                "~/areas/dashboard/Scripts/app/modules/gmap.js",
                "~/areas/dashboard/Scripts/app/modules/load-css.js",
                "~/areas/dashboard/Scripts/app/modules/localize.js",
                "~/areas/dashboard/Scripts/app/modules/maps-vector.js",
                "~/areas/dashboard/Scripts/app/modules/navbar-search.js",
                "~/areas/dashboard/Scripts/app/modules/notify.js",
                "~/areas/dashboard/Scripts/app/modules/now.js",
                "~/areas/dashboard/Scripts/app/modules/panel-tools.js",
                "~/areas/dashboard/Scripts/app/modules/play-animation.js",
                "~/areas/dashboard/Scripts/app/modules/porlets.js",
                "~/areas/dashboard/Scripts/app/modules/sidebar.js",
                "~/areas/dashboard/Scripts/app/modules/skycons.js",
                "~/areas/dashboard/Scripts/app/modules/slimscroll.js",
                "~/areas/dashboard/Scripts/app/modules/sparkline.js",
                "~/areas/dashboard/Scripts/app/modules/table-checkall.js",
                "~/areas/dashboard/Scripts/app/modules/toggle-state.js",
                "~/areas/dashboard/Scripts/app/modules/utils.js",
                "~/areas/dashboard/Scripts/app/modules/chart.js",
                "~/areas/dashboard/Scripts/app/modules/morris.js",
                "~/areas/dashboard/Scripts/app/modules/rickshaw.js",
                "~/areas/dashboard/Scripts/app/modules/chartist.js"
            ));
        }

        private static void RegisterAdminVendorBundles(BundleCollection bundles)
        {
            // Vendor Plugins

            bundles.Add(new ScriptBundle("~/dashboard/sparklines").Include(
                "~/areas/dashboard/Vendor/sparklines/jquery.sparkline.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/ChartJS").Include(
                 "~/areas/dashboard/Vendor/Chart.js/Chart.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/simpleLineIcons").Include(
              "~/areas/dashboard/Vendor/simple-line-icons/css/simple-line-icons.css"));

            bundles.Add(new ScriptBundle("~/dashboard/storage").Include(
              "~/areas/dashboard/Vendor/jQuery-Storage-API/jquery.storageapi.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/jqueryEasing").Include(
              "~/areas/dashboard/Vendor/jquery.easing/js/jquery.easing.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/datatables").Include(
              "~/areas/dashboard/Vendor/datatables/media/js/jquery.dataTables.min.js",
              "~/areas/dashboard/Vendor/datatables-colvis/js/dataTables.colVis.js",
              "~/areas/dashboard/Vendor/datatable-bootstrap/js/dataTables.bootstrap.js",
              "~/areas/dashboard/Vendor/datatable-bootstrap/js/dataTables.bootstrapPagination.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/datatablesCss")
              .Include("~/areas/dashboard/Vendor/datatables-colvis/css/dataTables.colVis.css")
              .Include("~/areas/dashboard/Vendor/datatable-bootstrap/css/dataTables.bootstrap.css")
            );

            bundles.Add(new ScriptBundle("~/dashboard/parsley").Include(
              "~/areas/dashboard/Vendor/parsleyjs/dist/parsley.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/filestyle").Include(
              "~/areas/dashboard/Vendor/bootstrap-filestyle/src/bootstrap-filestyle.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/tagsinput").Include(
              "~/areas/dashboard/Vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/tagsinputCss").Include(
              "~/areas/dashboard/Vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/gmap").Include(
              "~/areas/dashboard/Vendor/jQuery-gMap/jquery.gmap.min.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/weatherIcons").Include(
              "~/areas/dashboard/Vendor/weather-icons/css/weather-icons.min.css"));

            bundles.Add(new ScriptBundle("~/dashboard/skycons").Include(
              "~/areas/dashboard/Vendor/skycons/skycons.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/whirl").Include(
              "~/areas/dashboard/Vendor/whirl/dist/whirl.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/classyloader").Include(
              "~/areas/dashboard/Vendor/jquery-classyloader/js/jquery.classyloader.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/animo").Include(
              "~/areas/dashboard/Vendor/animo.js/animo.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/fastclick").Include(
              "~/areas/dashboard/Vendor/fastclick/lib/fastclick.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/fontawesome").Include(
              "~/areas/dashboard/Vendor/fontawesome/css/font-awesome.min.css"));


            bundles.Add(new ScriptBundle("~/dashboard/sliderCtrl").Include(
              "~/areas/dashboard/Vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/sliderCtrlCss").Include(
              "~/areas/dashboard/Vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/wysiwyg").Include(
              "~/areas/dashboard/Vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js",
              "~/areas/dashboard/Vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/slimscroll").Include(
              "~/areas/dashboard/Vendor/slimscroll/jquery.slimscroll.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/screenfull").Include(
              "~/areas/dashboard/Vendor/screenfull/dist/screenfull.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/jvectormap").Include(
              "~/areas/dashboard/Vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js",
              "~/areas/dashboard/Vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js",
              "~/areas/dashboard/Vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/jvectormapCss").Include(
              "~/areas/dashboard/Vendor/ika.jvectormap/jquery-jvectormap-1.2.2.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/flot").Include(
              "~/areas/dashboard/Vendor/flot/jquery.flot.js",
              "~/areas/dashboard/Vendor/flot.tooltip/js/jquery.flot.tooltip.min.js",
              "~/areas/dashboard/Vendor/flot/jquery.flot.resize.js",
              "~/areas/dashboard/Vendor/flot/jquery.flot.pie.js",
              "~/areas/dashboard/Vendor/flot/jquery.flot.time.js",
              "~/areas/dashboard/Vendor/flot/jquery.flot.categories.js",
              "~/areas/dashboard/Vendor/flot-spline/js/jquery.flot.spline.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/jqueryUi").Include(
                // "~/areas/dashboard/Vendor/jquery-ui/ui/*.js",
                "~/areas/dashboard/Vendor/jquery-ui/ui//core.js",
                "~/areas/dashboard/Vendor/jquery-ui/ui//widget.js",
                "~/areas/dashboard/Vendor/jquery-ui/ui//mouse.js",
                "~/areas/dashboard/Vendor/jquery-ui/ui//draggable.js",
                "~/areas/dashboard/Vendor/jquery-ui/ui//droppable.js",
                "~/areas/dashboard/Vendor/jquery-ui/ui//sortable.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/jqueryUiTouchPunch").Include(
              "~/areas/dashboard/Vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/moment").Include(
              "~/areas/dashboard/Vendor/moment/min/moment-with-locales.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/inputmask").Include(
              "~/areas/dashboard/Vendor/jquery.inputmask/dist/jquery.inputmask.bundle.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/flatdoc").Include(
              "~/areas/dashboard/Vendor/flatdoc/flatdoc.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/chosen").Include(
              "~/areas/dashboard/Vendor/chosen_v1.2.0/chosen.jquery.min.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/chosenCss").Include(
              "~/areas/dashboard/Vendor/chosen_v1.2.0/chosen.min.css"));

            bundles.Add(new ScriptBundle("~/dashboard/fullcalendar").Include(
              "~/areas/dashboard/Vendor/fullcalendar/dist/fullcalendar.min.js",
              "~/areas/dashboard/Vendor/fullcalendar/dist/gcal.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/fullcalendarCss").Include(
              "~/areas/dashboard/Vendor/fullcalendar/dist/fullcalendar.css"
            ));

            bundles.Add(new StyleBundle("~/dashboard/animatecss").Include(
              "~/areas/dashboard/Vendor/animate.css/animate.min.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/localize").Include(
              "~/areas/dashboard/Vendor/jquery-localize-i18n/dist/jquery.localize.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/nestable").Include(
              "~/areas/dashboard/Vendor/nestable/jquery.nestable.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/sortable").Include(
              "~/areas/dashboard/Vendor/html.sortable/dist/html.sortable.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/jqGrid").Include(
              "~/areas/dashboard/Vendor/jqgrid/js/jquery.jqGrid.js",
              "~/areas/dashboard/Vendor/jqgrid/js/i18n/grid.locale-en.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/jqGridCss").Include(
                "~/areas/dashboard/Vendor/jqgrid/css/ui.jqgrid.css",
                "~/areas/dashboard/Vendor/jquery-ui/themes/smoothness/jquery-ui.css"
            ));

            bundles.Add(new StyleBundle("~/dashboard/fileUploadCss").Include(
                "~/areas/dashboard/Vendor/blueimp-file-upload/css/jquery.fileupload.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/fileUpload").Include(
                "~/areas/dashboard/Vendor/jquery-ui/ui/widget.js",
                "~/areas/dashboard/Vendor/blueimp-tmpl/js/tmpl.js",
                "~/areas/dashboard/Vendor/blueimp-load-image/js/load-image.all.min.js",
                "~/areas/dashboard/Vendor/blueimp-canvas-to-blob/js/canvas-to-blob.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.iframe-transport.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.fileupload.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.fileupload-process.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.fileupload-image.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.fileupload-audio.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.fileupload-video.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.fileupload-validate.js",
                "~/areas/dashboard/Vendor/blueimp-file-upload/js/jquery.fileupload-ui.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/xEditableCss").Include(
                "~/areas/dashboard/Vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/xEditable").Include(
              "~/areas/dashboard/Vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/jqueryValidate").Include(
              "~/areas/dashboard/Vendor/jquery-validation/dist/jquery.validate.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/jquerySteps").Include(
              "~/areas/dashboard/Vendor/jquery.steps/build/jquery.steps.js"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/datetimePicker").Include(
              "~/areas/dashboard/Vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/datetimePickerCss").Include(
                "~/areas/dashboard/Vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"
            ));

            bundles.Add(new StyleBundle("~/dashboard/RickshawCss").Include(
                "~/areas/dashboard/Vendor/rickshaw/rickshaw.min.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/Rickshaw").Include(
              "~/areas/dashboard/Vendor/d3/d3.min.js",
              "~/areas/dashboard/Vendor/rickshaw/rickshaw.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/ChartistCss").Include(
                "~/areas/dashboard/Vendor/chartist/dist/chartist.min.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/Chartist").Include(
              "~/areas/dashboard/Vendor/chartist/dist/chartist.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/MorrisCss").Include(
                "~/areas/dashboard/Vendor/morris.js/morris.css"
            ));

            bundles.Add(new ScriptBundle("~/dashboard/Morris").Include(
              "~/areas/dashboard/Vendor/raphael/raphael.js",
              "~/areas/dashboard/Vendor/morris.js/morris.js"
            ));

            bundles.Add(new StyleBundle("~/dashboard/Spinkit").Include(
                "~/areas/dashboard/Vendor/spinkit/css/spinkit.css"
            ));

            bundles.Add(new StyleBundle("~/dashboard/LoadersCss").Include(
                "~/areas/dashboard/Vendor/loaders.css/loaders.css"
            ));
        }
    }
}