﻿"use strict";

angular.module('cpApp.sidebar.controllers', []).controller('SidebarController', function ($scope, $http) {
    $scope.loading = true;
    $scope.editMode = false;

    $http.get('/api/sidebar/get').success(function (data) {
        $scope.menuitems = data.cpObject;
        $scope.loading = false;
    })
    .error(function () {
        $scope.error = "An error has occured while loading the menu items!";
        $scope.loading = false;
    });
});