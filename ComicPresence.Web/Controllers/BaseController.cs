﻿using System.Web.Mvc;

using ComicPresence.Infrastructure;
using ComicPresence.Model;
using ComicPresence.Model.Repositories;
using ComicPresence.Repositories;
using ComicPresence.Repositories.EF;
using ComicPresence.Repositories.EF.Repositories;

namespace ComicPresence.Web.Controllers
{
    public class BaseController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BaseController(): this(new EFUnitOfWorkFactory())
        {

        }

        public BaseController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }
    }
}
