﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using ComicPresence.Infrastructure;
using ComicPresence.Model;
using ComicPresence.Model.Repositories;
using ComicPresence.Repositories;

namespace ComicPresence.Web.Controllers
{
    public class ComicController : BaseController
    {
        private readonly IComicRepository _comicRepository;

        public ComicController(IComicRepository comicRepository)
        {
            _comicRepository = comicRepository;            
        }

        //
        // GET: /Comic/

        public ActionResult Index()
        {
            var comics = _comicRepository.GetAllComics();
            return View(comics);
        }

        public ActionResult Comic(string id)
        {
            return View(_comicRepository.GetComicBySlug(id));
        }

        public ActionResult _ComicDisplay()
        {
            return PartialView(_comicRepository.GetCurrentComic());
        }
    }
}
