﻿using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using log4net;
using log4net.Config;

using ComicPresence.Common.Logging;

[assembly: WebActivator.PreApplicationStartMethod(typeof(ComicPresence.Web.LogStartup), "Start")]

namespace ComicPresence.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        public Logger log = new Logger();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log.Info(string.Format("Comic-Presence CMS started on --- {0}", DateTime.Now));
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError().GetBaseException();
            log.Error(exception.Message, exception);
        }
    }

    public static class LogStartup
    {
        public static void Start()
        {
            XmlConfigurator.Configure();            
        }
    }
}