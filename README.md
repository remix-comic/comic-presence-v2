#Comic Presence CMS
`Version 2.0.0.0`

Brett G. Murphy

Project Lead

brett@nodoubt223rd.com

Comic Presence is "Content Management System" geared towards the creation and publication of web comics,
without all the bloated extra features you may not need from other types of CMS's.

### Features ###
* Post dated comic entries
* Grouping of comics by book or chapters
* Transcripts
* Custom archive navigation
* Easy site theme creation
* Character customization
	* By Location
	* By number of appearances
	* Listing per character of each comic they appear in.