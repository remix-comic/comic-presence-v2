﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using FluentAssertions;

using ComicPresence.Model;
using ComicPresence.Infrastructure;
using ComicPresence.Repositories.EF;

namespace ComicPresence.Tests.Unit
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class CmsContextTests : IntegrationTestBase
    {
        [TestMethod]
        public void CanExecuteQuery()
        {
            var context = DataContextFactory.GetDataContext();
            var comics = context.Comics.Where(c => c.OnHomePage == false).ToList();

            comics.Should().NotBeNull();
            
            context.Should().NotBeNull();
        }
    }
}
