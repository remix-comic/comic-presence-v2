﻿using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;

using ComicPresence.Repositories.EF;

namespace ComicPresence.Tests.Unit
{
    [ExcludeFromCodeCoverage]
    public class IntegrationTestBase
    {
        internal IntegrationTestBase()
        {
            DataContextFactory.GetDataContext();
        }
    }
}
